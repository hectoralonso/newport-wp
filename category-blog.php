<?php
/**
 * The template for displaying Category pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header("blog"); ?>


<section id="cuerpo" class="bg-color-p-0100 padding-t-50 padding-b-20">
    <div class="container">

		<?php
			if ( have_posts() ) : while ( have_posts() ) : the_post();

    		$thumb_id = get_post_thumbnail_id();
			$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'newport', true);
			$thumb_url = $thumb_url_array[0];

     	?>
	    	<div class="row">
				<article class="blog-article padding-t-16 padding-b-16 overflow-h margin-b-30">
					<?php // the_post_thumbnail(newport); ?>
					
				    <div class="col-md-4">
				    	<?php if ( has_post_thumbnail() ) { ?>
	                    	<a href="<?php the_permalink() ?>"><img class="img-responsive margin-b-16" src="<?php echo $thumb_url; ?>" alt="<?php  the_title(); ?>"></a>
	                    <?php } else { } ?>
				    </div>
				    <div class="col-md-8">
				    	<span class="font-size-xs font-weight-l text-align-l color-w-0100 display-b margin-b-10">
				    		<?php echo esc_html( get_the_date() ); ?>
				    	</span>
				    	<header class="margin-b-10">
						    <h2 class="blog-titular font-size-l font-weight-b text-align-i color-w-0100"><a class="blog-titular-a" href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
						</header>
				      	<?php the_excerpt(); ?>
				    </div>
				</article>
			</div>

		<?php endwhile; ?>
		<?php endif; ?>

	</div>
</section>


<?php 
get_footer(); 
?>