<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>



<div>
</div>



<footer id="pie" class="bg-color-p-0100 bg-0 overflow-h padding-t-80">
  <div class="container">
    <div class="row">

      <div id="pie-arriba" class="overflow-h linea-horizontal-b">
        <div class="col-md-6">

          <h2 class="font-size-m font-weight-b text-align-l color-w-0100 margin-b-10">Dirección</h2>
          <p class="font-size-s font-weight-n text-align-l color-w-0100">C/ Michel Faraday, 75, naves 9 y 10</p>
          <p class="font-size-s font-weight-n text-align-l color-w-0100 luto-izq margin-b-30">33211 Gijón, Asturias</p>

          <h2 class="font-size-m font-weight-b text-align-l color-w-0100 margin-b-10">Horario</h2>
          <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-5">Lunes, martes, miércoles y jueves de 9:00 a 18:00.</p>
          <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-5">Viernes de 9:00 a 15:00.</p>
          <p class="font-size-s font-weight-n text-align-l color-w-0100 luto-izq margin-b-30 margin-b-5">Sábados y domingo cerrado.</p>

          <a class="font-size-m font-weight-b color-p-0100 display-b text-align-c bg-color-w-080 padding-t-20 padding-r-10 padding-b-20 padding-l-10 margin-b-40" href="tel:900921948">Teléfono gratuito:  900 921 948</a>

        </div>

        <div class="col-md-6">
          <iframe class="width-0100 height-470 margin-b-20" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2893.576963238493!2d-5.686318684506849!3d43.51115987912641!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd367cdecbd0656d%3A0x5b973d607ea50e67!2sCalle+Michel+Faraday%2C+75%2C+33211%2C+Asturias!5e0!3m2!1ses!2ses!4v1506953345139" allowfullscreen></iframe>
        </div>
      </div>
      <!-- FIN pie arriba -->

    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="padding-t-20 padding-b-30">
          <div class="copy float-l padding-r-16">
            <p class="font-size-xs padding-t-10 padding-b-10 font-weight-l text-align-l color-w-0100"><a class="link-corrido" href="<?php echo get_template_directory_uri(); ?>/aviso-legal.pdf" target="_blank">© 2017 Newport</a> <span class="font-size-xxs font-weight-l padding-l-6 padding-r-6">|</span> <a class="link-corrido" href="<?php echo get_template_directory_uri(); ?>/politica-privacidad.pdf" target="_blank">Política de privacidad</a> <span class="font-size-xxs font-weight-l padding-l-6 padding-r-6">|</span> <a class="link-corrido" href="<?php echo get_template_directory_uri(); ?>/politica-cookies.pdf" target="_blank">Política de cookies</a> <span class="font-size-xxs font-weight-l padding-l-6 padding-r-6">|</span> <a class="link-corrido" href="http://www.simbiosys.es/" target="_blank">Diseño Web Simbiosys</a></p>
          </div>
          <!-- <div class="redes-sociale padding-b-30 float-l">
            <ul>
              <li class="display-i padding-r-16"><a href="#" target="_blank"><img src="img/icono-twitter.png" alt="Icono Twitter"></a></li>
              <li class="display-i padding-r-16"><a href="#" target="_blank"><img src="img/icono-facebook.png" alt="Icono Facebook"></a></li>
              <li class="display-i padding-r-16"><a href="#" target="_blank"><img src="img/icono-google-plus.png" alt="Icono Google Plus"></a></li>
            </ul>
          </div> -->
        </div>
      </div>
    </div>

  </div>
  <!-- FIN container -->
</footer>
<!-- FIN pie -->


<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/jquery-3.2.1.min.js"></script>

<!-- owlcarousel JS -->
<script src="<?php echo get_template_directory_uri(); ?>/owlcarousel/owl.carousel.min.js"></script>

<!-- simplelightbox JS -->
<script src="<?php echo get_template_directory_uri(); ?>/js/simpleLightbox.min.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/bootstrap.min.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/main-owl.js"></script>


<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/59ce2a62c28eca75e462317d/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

<?php wp_footer(); ?>
</body>
</html>
