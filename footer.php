<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>



<div>
</div>



<footer id="pie" class="bg-color-p-0100 bg-0 overflow-h padding-t-80">
  <div class="container">
    <div class="row">

        <div id="pie-arriba" class="overflow-h linea-horizontal-b">
          <div class="col-md-8 col-lg-6">
            <div class="row">
              <div class="col-md-12">
                <p class="font-size-l font-weight-b text-align-l color-w-0100 margin-b-50">Aproveche la oferta de lanzamiento en España con descuentos de hasta el 15%</p>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <a class="btn-s-b display-b text-align-c margin-b-30" href="<?php echo get_site_url(); ?>/presupuesto-gratuito">Presupuesto gratuito</a>
              </div>
              <div class="col-md-6">
                <a class="btn-s-b display-b text-align-c margin-b-30" href="<?php echo get_site_url(); ?>/contacto">Contacto</a>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <a class="font-size-m font-weight-b color-p-0100 display-b text-align-c bg-color-w-080 padding-t-20 padding-r-10 padding-b-20 padding-l-10 margin-b-40" href="tel:900921948">Teléfono gratuito:  900 921 948</a>
              </div>
            </div>
          </div>
        <div class="hidden-sm-down col-md-4 offset-lg-1 col-lg-3">
          <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/descuento-oferta-calefaccion.png" alt="Descuento oferta calefacción">
        </div>
      	</div>
      	<!-- FIN pie arriba -->

    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="padding-t-20 padding-b-30">
          <div class="copy float-l padding-r-16">
            <p class="font-size-xs padding-t-10 padding-b-10 font-weight-l text-align-l color-w-0100"><a class="link-corrido" href="<?php echo get_template_directory_uri(); ?>/aviso-legal.pdf" target="_blank">© 2017 Newport</a> <span class="font-size-xxs font-weight-l padding-l-6 padding-r-6">|</span> <a class="link-corrido" href="<?php echo get_template_directory_uri(); ?>/politica-privacidad.pdf" target="_blank">Política de privacidad</a> <span class="font-size-xxs font-weight-l padding-l-6 padding-r-6">|</span> <a class="link-corrido" href="<?php echo get_template_directory_uri(); ?>/politica-cookies.pdf" target="_blank">Política de cookies</a> <span class="font-size-xxs font-weight-l padding-l-6 padding-r-6">|</span> <a class="link-corrido" href="http://www.simbiosys.es/" target="_blank">Diseño Web Simbiosys</a></p>
          </div>
          <!-- <div class="redes-sociale padding-b-30 float-l">
            <ul>
              <li class="display-i padding-r-16"><a href="#" target="_blank"><img src="img/icono-twitter.png" alt="Icono Twitter"></a></li>
              <li class="display-i padding-r-16"><a href="#" target="_blank"><img src="img/icono-facebook.png" alt="Icono Facebook"></a></li>
              <li class="display-i padding-r-16"><a href="#" target="_blank"><img src="img/icono-google-plus.png" alt="Icono Google Plus"></a></li>
            </ul>
          </div> -->
        </div>
      </div>
    </div>

  </div>
  <!-- FIN container -->
</footer>
<!-- FIN pie -->


<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/jquery-3.2.1.min.js"></script>

<!-- owlcarousel JS -->
<script src="<?php echo get_template_directory_uri(); ?>/owlcarousel/owl.carousel.min.js"></script>

<!-- simplelightbox JS -->
<script src="<?php echo get_template_directory_uri(); ?>/js/simpleLightbox.min.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/bootstrap.min.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/main-owl.js"></script>


<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/59ce2a62c28eca75e462317d/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

<?php wp_footer(); ?>
</body>
</html>
