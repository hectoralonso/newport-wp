<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?>

<!DOCTYPE html>

<html itemscope itemtype="http://schema.org/Corporation" <?php language_attributes(); ?> class="no-js">

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Newport. Radiadores integrables con la mejor estética y el máximo confort. Aerotermia y la Energía Solar, que le proporcionará agua caliente de un modo eficaz y económico">
    <meta name="author" content="Simbiosys">
    <link rel="icon" href="#">

    <title>Newport. Calefacción que reúne lo mejor de todos los sistemas. Calor híbrido</title>

    <!-- bootstrap & simbiosys base core CSS -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/screen.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet"> 

    <!-- owlcarousel CSS -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/owlcarousel/owl.theme.default.min.css">

    <!-- simplelightbox CSS -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/simpleLightbox.min.css">
    
    <!-- fav touch icons -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/favicons/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- FIN Fav and touch icons -->

    <link type="text/plain" rel="author" href="<?php echo get_template_directory_uri(); ?>/humans.txt">

    <!--[if lt IE 9]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script>window.html5 || document.write('<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/html5shiv.js"><\/script>')</script>
    <![endif]-->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-110025816-1"></script>
    <script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-110025816-1');
    </script>

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header id="cabecera" class="estanteria-carrusel position-r">
      
  <div id="tel-gratis" class="fijo-arriba bg-color-p-0100 text-align-c margin-b-30">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <a class="color-w-0100 font-size-s font-weight-l display-ib padding-tel0 hover-w" href="tel:900921948"><span class="font-size-tel1">Teléfono gratuito: </span> <span class="font-size-tel2">900 921 948</span></a>
        </div>
      </div>
    </div>
  </div>
  <!-- FIN tel-gratis -->

  <a id="btn-presu-gratis-xs" class="hidden-sm-up btn-presu-gratis-xs btn-s-b margin-t-30" href="<?php echo get_site_url(); ?>/presupuesto-gratuito">Presupuesto gratuito</a>

  <div id="btn-estudio-gratis" class="hidden-xs-down giro-270 btn-estudio-gratis">
    <a class="btn-s-b" href="#">Presupuesto gratuito</a>
  </div>
  <aside id="estudio-gratis" class="estudio-gratis">
    <div class="padding-20 bg-color-w-0100 width-260 text-align-c">
        <div class="dispaly-b text-align-r margin-b-10">
          <a id="aspa-estudio" class="padding-7" href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/aspa.png" alt="Icono aspa"></a>
        </div>
        <h4 class="color-p-0100 font-weight-l font-size-l margin-b-30">¿Qué podemos hacer por ti?</h4>
        <a class="btn-s-b display-b text-align-c margin-b-30" href="<?php echo get_site_url(); ?>/presupuesto-gratuito">Presupuesto gratuito</a>
        <a class="btn-s-a display-b text-align-c margin-b-30" href="<?php echo get_site_url(); ?>/contacto">Solicita catálogo</a>
        <span class="display-b color-p-0100 font-weight-b font-size-l margin-b-10">Llama gratis</span>
        <a href="tel:900921948"><span class="display-b c-secondary font-weight-b font-size-l margin-b-20">900 921 948</span></a>
        <span class="display-b color-p-0100 font-weight-l font-size-l margin-b-20">Estudiaremos tus necesidades de calefacción</span>
    </div>
  </aside>
  <!-- FIN estudio-gratis -->

  <div id="menu-hamburguesa" class="menu-hamburguesa hidden-lg-up fijo-derecha">
    <a class="#" href="#">
      <img src="<?php echo get_template_directory_uri(); ?>/img/menu-movil.png" alt="Menú móvil">
    </a>
  </div>
  <!-- FIN menu-hamburguesa -->

  <nav id="menu-movil" class="menu-movil">
        <div class="display-f align-items-c justify-content-c height-0100">
          <ul>
            <li><a id="cerrar-menu-movil" href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/aspa.png" alt="Icono aspa"></a></li>
            <li class="menu-movil-li">
              <a class="menu-movil-a" href="<?php echo get_site_url(); ?>">Portada</a>
            </li>
            <li class="menu-movil-li">
              <a class="menu-movil-a" href="<?php echo get_site_url(); ?>/sobre-newport">Sobre Newport</a>
            </li>
            <li class="menu-movil-li">
              <a class="menu-movil-a" href="<?php echo get_site_url(); ?>/radiadores-piso">Radiadores Piso</a>
            </li>
            <li class="menu-movil-li">
              <a class="menu-movil-a" href="<?php echo get_site_url(); ?>/lineas-de-bano-piso">Línea de Baño Piso</a>
            </li>
            <li class="menu-movil-li">
              <a class="menu-movil-a" href="<?php echo get_site_url(); ?>/agua-caliente-y-calefaccion-piso">Agua Caliente y Calefacción Piso</a>
            </li>
            <li class="menu-movil-li">
              <a class="menu-movil-a" href="<?php echo get_site_url(); ?>/radiadores-casa">Radiadores Casa</a>
            </li>
            <li class="menu-movil-li">
              <a class="menu-movil-a" href="<?php echo get_site_url(); ?>/lineas-de-bano-casa">Línea de Baño Casa</a>
            </li>
            <li class="menu-movil-li">
              <a class="menu-movil-a" href="<?php echo get_site_url(); ?>/agua-caliente-y-calefaccion-casa">Agua Caliente y Calefacción Casa</a>
            </li>
            <li class="menu-movil-li">
              <a class="menu-movil-a" href="<?php echo get_site_url(); ?>/agua-caliente">Agua Caliente</a>
            </li>
            <li class="menu-movil-li">
              <a class="menu-movil-a" href="<?php echo get_site_url(); ?>/blog">Blog</a>
            </li>
            <li class="menu-movil-li">
              <a class="menu-movil-a" href="<?php echo get_site_url(); ?>/contacto">Contacto</a>
            </li>
          </ul>
        </div>
      </nav>
      <!-- FIN menu movil -->

  <div id="menu-logos" class="menu-logos">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <nav id="menu-principal" class="margin-b-50 hidden-md-down">
              <ul class="font-size-xs text-align-c text-trans-u">
                  <li class="display-i"><a class="menu-link menu-activo padding-l-14 padding-r-14" href="<?php echo get_site_url(); ?>">Portada</a></li>
                  <li class="display-i"><a class="menu-link padding-l-14 padding-r-14" href="<?php echo get_site_url(); ?>/sobre-newport">Sobre Newport</a></li>
                  <li class="display-i"><a class="menu-link padding-l-14 padding-r-14" href="<?php echo get_site_url(); ?>/radiadores-piso">Soluciones piso</a>
                    <ul>
                      <li><a href="<?php echo get_site_url(); ?>/radiadores-piso">Radiadores</a></li>
                      <li><a href="<?php echo get_site_url(); ?>/lineas-de-bano-piso">Línea de Baño</a></li>
                      <li><a href="<?php echo get_site_url(); ?>/agua-caliente-y-calefaccion-piso">Agua Caliente y Calefacción</a></li>
                    </ul>
                  </li>
                  <li class="display-i"><a class="menu-link padding-l-14 padding-r-14" href="<?php echo get_site_url(); ?>/radiadores-casa">Soluciones casa</a>
                    <ul>
                      <li><a href="<?php echo get_site_url(); ?>/radiadores-casa">Radiadores</a></li>
                      <li><a href="<?php echo get_site_url(); ?>/lineas-de-bano-casa">Línea de Baño</a></li>
                      <li><a href="<?php echo get_site_url(); ?>/agua-caliente-y-calefaccion-casa">Agua Caliente y Calefacción</a></li>
                    </ul>
                  </li>
                  <li class="display-i"><a class="menu-link padding-l-14 padding-r-14" href="<?php echo get_site_url(); ?>/agua-caliente">Agua Caliente</a></li>
                  <li class="display-i"><a class="menu-link padding-l-14 padding-r-14" href="<?php echo get_site_url(); ?>/blog">Blog</a></li>
                  <li class="display-i"><a class="menu-link padding-l-14 padding-r-14" href="<?php echo get_site_url(); ?>/contacto">Contacto</a></li>
              </ul>
          </nav>
        </div>
      </div>

      <div class="row hidden-md-down">
        <div class="col-md-12">
          <a id="logo" class="display-ib margin-b-30" href="<?php echo get_site_url() ?>">
            <img class="z-index-9999999 position-r" src="<?php echo get_template_directory_uri(); ?>/img/newport-eco-hybrid-system-logo.png" alt="Newport logo calor híbrido">
          </a>
        </div>
      </div>

      <a id="logo-negativo" class="logo-negativo hidden-xs-down hidden-lg-up" href="<?php echo get_site_url() ?>">
        <img src="<?php echo get_template_directory_uri(); ?>/img/newport-eco-hybrid-system-logo-2.png" alt="Newport logo calor híbrido">
      </a>
      <a id="logo-isotipo" class="logo-negativo hidden-sm-up" href="<?php echo get_site_url() ?>">
        <img src="<?php echo get_template_directory_uri(); ?>/img/newport-eco-hybrid-system-isotipo.png" alt="Newport logo calor híbrido">
      </a>
    </div>
  </div>
  <!-- FIN menu y logos -->

  <div class="owl-carousel owl-theme owl-portada">

  	<!-- diapo 1 -->
    <div class="diapo-carrusel alto-ventana bg-1">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1 class="padding-movil font-size-xxxl font-weight-b color-p-0100 margin-b-4">Calor híbrido <span class="font-size-s">by Newport</span></h1>
          </div>
        </div>
        <div class="row">
          <div class="col-md-8 col-lg-6">
              <p class="font-size-l color-p-0100 margin-b-10"><strong>La calefacción que reúne lo mejor de todos los sistemas</strong></p>
              <p class="font-size-s color-p-0100 font-style-i margin-b-30 hidden-xs-down">La mejor solución para su hogar, con radiadores integrables de última generación, que le ofrecerán la mejor estética y el máximo confort. Junto con la aerotermia y la energía solar, que le proporcionará agua caliente de un modo eficaz y económico.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-8 col-lg-6">
            <div class="row">
              <div class="col-md-6">
                <a class="btn-s-b display-b text-align-c margin-b-30" href="<?php echo get_site_url(); ?>/radiadores-piso">Ver radiadores</a>
              </div>
              <div class="col-md-6">
                <a class="btn-s-b display-b text-align-c margin-b-30" href="c<?php echo get_site_url(); ?>/contacto">Contacto</a>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <a class="font-size-m font-weight-b color-p-0100 display-b text-align-c bg-color-w-080 padding-t-20 padding-r-10 padding-b-20 padding-l-10 margin-b-10" href="tel:900921948">
                  <p class="font-size-s text-align-c color-p-0100 margin-b-10"><strong class="c-secondary">Llame ahora y nos ocupamos de todo</strong></p>
                  <span>Teléfono gratuito:  900 921 948</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- FIN container -->
    </div>
    <!-- FIN diapo 1 -->

    <!-- diapo 2 -->
    <div class="diapo-carrusel alto-ventana bg-tecnico">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h6 class="padding-movil font-size-xxxl font-weight-b color-p-0100 margin-b-4">Cero problemas <span class="font-size-s hidden-sm-down">con Newport</span></h6>
            <span class="font-size-s display-b hidden-md-up font-size-s font-weight-b padding-t-5 padding-b-5">con Newport</span>
          </div>
        </div>
        <div class="row">
          <div class="col-md-8 col-lg-6">
              <p class="font-size-l color-p-0100 margin-b-10"><strong>Llámenos y nos encargamos de todo</strong></p>
              <p class="font-size-s color-p-0100 font-style-i margin-b-30 hidden-xs-down">Nuestro técnicos le presentarán un presupuesto con todo incluido para que usted sepa cuánto le costará su instalación llave en mano, sin ningún tipo de sorpresa adicional.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-8 col-lg-6">
            <div class="row">
              <div class="col-md-6">
                <a class="btn-s-b display-b text-align-c margin-b-30" href="<?php echo get_site_url(); ?>/radiadores-piso">Ver radiadores</a>
              </div>
              <div class="col-md-6">
                <a class="btn-s-b display-b text-align-c margin-b-30" href="c<?php echo get_site_url(); ?>/contacto">Contacto</a>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <a class="font-size-m font-weight-b color-p-0100 display-b text-align-c bg-color-w-080 padding-t-20 padding-r-10 padding-b-20 padding-l-10 margin-b-10" href="tel:900921948">
                  <p class="font-size-s text-align-c color-p-0100 margin-b-10"><strong class="c-secondary">Llame ahora y nos ocupamos de todo</strong></p>
                  <span>Teléfono gratuito:  900 921 948</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- FIN container -->
    </div>
    <!-- FIN diapo 2 -->

    <!-- diapo 3 -->
    <div class="diapo-carrusel alto-ventana bg-oferta">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h6 class="padding-movil font-size-xxxl-b font-weight-b color-p-0100 margin-b-4">Descuentos de hasta el 15%</h6>
          </div>
        </div>
        <div class="row">
          <div class="col-md-8 col-lg-6">
              <p class="font-size-l color-p-0100 margin-b-10"><strong>Aproveche la oferta de lanzamiento y nuestra financiación a medida, con soluciones desde 30 euros al mes</strong></p>
              <!-- <p class="font-size-s color-p-0100 font-style-i margin-b-30 hidden-xs-down">In malesuada lobortis justo, sed molestie eros convallis in. Donec tincidunt arcu non sapien fermentum, sed accumsan felis suscipit. Vestibulum ornare tincidunt luctus. Nunc posuere, lectus vitae bibendum rhoncus, urna tortor tincidunt.</p> -->
          </div>
        </div>
        <div class="row">
          <div class="col-md-8 col-lg-6">
            <div class="row">
              <div class="col-md-6">
                <a class="btn-s-b display-b text-align-c margin-b-30" href="<?php echo get_site_url(); ?>/radiadores-piso">Ver radiadores</a>
              </div>
              <div class="col-md-6">
                <a class="btn-s-b display-b text-align-c margin-b-30" href="c<?php echo get_site_url(); ?>/contacto">Contacto</a>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <a class="font-size-m font-weight-b color-p-0100 display-b text-align-c bg-color-w-080 padding-t-20 padding-r-10 padding-b-20 padding-l-10 margin-b-10" href="tel:900921948">
                  <p class="font-size-s text-align-c color-p-0100 margin-b-10"><strong class="c-secondary">Llame ahora y nos ocupamos de todo</strong></p>
                  <span>Teléfono gratuito:  900 921 948</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- FIN container -->
    </div>
    <!-- FIN diapo 3 -->

    <!-- diapo 4 -->
    <div class="diapo-carrusel alto-ventana bg-12">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h3 class="padding-movil font-size-xxxl font-weight-b color-p-0100 margin-b-4">Agua caliente</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-md-8 col-lg-6">
              <!-- <p class="font-size-l color-p-0100 margin-b-10"><strong>Newport presenta la única solución para el agua caliente: el Ecotermo multitanque WIFI</strong></p>
              <p class="font-size-s color-p-0100 font-style-i margin-b-30 hidden-xs-down">El único capaz de suministrar hasta 900 litros de agua continua ocupando el mínimo espacio. Pídanos presupuesto y nos ocupamos de todo.</p> -->
              <p class="font-size-s color-p-0100 font-style-i margin-b-30 hidden-xs-down">Hasta 900 litros de agua continua con el mínimo espacio. Nuestro termo multitanque wifi puede instalarse dentro de un mueble de cocina.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-8 col-lg-6">
            <div class="row">
              <div class="col-md-6">
                <a class="btn-s-b display-b text-align-c margin-b-30" href="<?php echo get_site_url(); ?>/agua-caliente">Ver agua caliente</a>
              </div>
              <div class="col-md-6">
                <a class="btn-s-b display-b text-align-c margin-b-30" href="<?php echo get_site_url(); ?>/contacto">Contacto</a>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <a class="font-size-m font-weight-b color-p-0100 display-b text-align-c bg-color-w-080 padding-t-20 padding-r-10 padding-b-20 padding-l-10 margin-b-10" href="tel:900921948">
                  <p class="font-size-s text-align-c color-p-0100 margin-b-10"><strong class="c-secondary">Llame ahora y nos ocupamos de todo</strong></p>
                  <span>Teléfono gratuito:  900 921 948</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- FIN container -->
    </div>
    <!-- FIN diapo 4 -->

  </div>
  <!-- FIN owl-carousel -->

  
</header>
<!-- FIN cabecera -->
