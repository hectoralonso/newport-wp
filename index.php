<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header();

?>


<section id="cuerpo" class="bg-color-p-0100">
  <div class="container">

      <!-- article 01 -->
      <article class="articulo padding-b-50">
        <div class="row">
          <header class="art-cabecera">
            <div class="col-md-12">
              <h2 class="art-num num-01 font-size-xl font-weight-l text-align-c color-w-0100">Wifi</h2>
            </div>
          </header>
        </div>
        <!-- art-division -->
        <div class="art-cuerpo">
          <div class="row">
            <div class="col-md-12 offset-lg-2 col-lg-8">
              <p class="font-size-s font-weight-n text-align-c color-w-0100 margin-b-18"><strong class="c-secondary">Con nuestro sistema Wifi de Newport usted podrá:</strong></p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18"><strong class="c-secondary">Personalizar la temperatura</strong> de cada habitación en función de su uso, dimensiones y horario desde cualquier tablet o smartphone.</p>
              <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18"><strong class="c-secondary">Asignar la cantidad de energía</strong> a utilizar por día, estableciendo prioridades. <strong class="c-secondary">¡Evite sustos en su factura de energía!</strong></p>
              <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18 luto-izq"><strong class="c-secondary">Predecir</strong>. El sistema registra sus hábitos de uso, adaptando su funcionamiento programado del modo más óptimo posible y totalmente de forma automática.</p>
              <!-- <div class="row">
                <div class="col-sm-4">
                  <img class="img-responsive margin-b-18" src="img/wifi-calefaccion-salon.jpg" alt="Sistema wifi">
                </div>
                <div class="col-sm-4">
                  <img class="img-responsive margin-b-18" src="img/wifi-calefaccion-habitacion.jpg" alt="Sistema wifi">
                </div>
                <div class="col-sm-4">
                  <img class="img-responsive margin-b-18" src="img/wifi-calefaccion-habitacion-principal.jpg" alt="Sistema wifi">
                </div>
              </div> -->
            </div>
            <div class="col-md-6">
              <img class="img-responsive margin-b-18" src="<?php echo get_template_directory_uri(); ?>/img/sistema-wifi-personalizacion-temperatura-prioridades-predictivo.png" alt="Sistema wifi">
            </div>
          </div>

          <div class="row">
            <div class="col-md-12 offset-lg-3 col-lg-6">
              <div class="owl-carousel">
                  <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/wifi-calefaccion-salon.jpg" alt="Sistema wifi"></div>
                  <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/wifi-calefaccion-habitacion.jpg" alt="Sistema wifi"></div>
                  <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/toallero-wifi.jpg" alt="Sistema wifi"></div>
              </div>
            </div>
          </div>
          
          <!-- <div class="row">
            <div class="col-sm-4">
              <img class="img-responsive margin-b-18" src="img/wifi-calefaccion-salon.jpg" alt="Sistema wifi">
            </div>
            <div class="col-sm-4">
              <img class="img-responsive margin-b-18" src="img/wifi-calefaccion-habitacion.jpg" alt="Sistema wifi">
            </div>
            <div class="col-sm-4">
              <img class="img-responsive margin-b-18" src="img/wifi-calefaccion-habitacion-principal.jpg" alt="Sistema wifi">
            </div>
          </div> -->

          <div class="row">
            <div class="col-md-12 offset-lg-2 col-lg-8">
              <span class="display-b text-align-c margin-t-18">
                <a class="btn-s-b" href="<?php echo get_site_url(); ?>/informacion-tecnica">Información técnica</a>
              </span>
            </div>
          </div>
        </div>
      </article>
      <!-- FIN article 01 -->


      <!-- article 02 -->
      <article class="articulo padding-b-50">
        <div class="row">
          <header class="art-cabecera">
            <div class="col-md-12">
              <h2 class="art-num num-02 font-size-xl font-weight-l text-align-c color-w-0100">Control independiente por habitación</h2>
            </div>
          </header>
        </div>
        <!-- art-division -->
        <div class="art-cuerpo">
          <div class="row">
            <div class="col-md-12 offset-lg-2 col-lg-8">
              <p class="font-size-s font-weight-n text-align-c color-w-0100 margin-b-18"><strong class="c-secondary">¿Por qué calentar toda la casa a la misma temperatura?</strong></p>
              <p class="font-size-s font-weight-n text-align-c color-w-0100 luto-centro">Nuestras necesidades de calefacción son muy distintas dependiendo de cada habitación. Por qué calentar el dormitorio a 21 grados todo el día si verdaderamente sólo lo utilizamos durante la noche.</p>
            </div>
           </div>

          <div class="row">
            <div class="col-md-12 offset-lg-2 col-lg-8">
              <div class="owl-carousel owl-theme owl-horas">
                <div><img class="img-responsive padding-t-30" src="<?php echo get_template_directory_uri(); ?>/img/calefaccion-control-independiente-desayuno.png" alt="La temperatura de la calefacción de la casa distinta para cada habitación"></div>
                <div><img class="img-responsive padding-t-30" src="<?php echo get_template_directory_uri(); ?>/img/calefaccion-control-independiente-vacia.png" alt="La temperatura de la calefacción de la casa distinta para cada habitación"></div>
                <div><img class="img-responsive padding-t-30" src="<?php echo get_template_directory_uri(); ?>/img/calefaccion-control-independiente-tarde.png" alt="La temperatura de la calefacción de la casa distinta para cada habitación"></div>
                <div><img class="img-responsive padding-t-30" src="<?php echo get_template_directory_uri(); ?>/img/calefaccion-control-independiente-cena.png" alt="La temperatura de la calefacción de la casa distinta para cada habitación"></div>
                <div><img class="img-responsive padding-t-30" src="<?php echo get_template_directory_uri(); ?>/img/calefaccion-control-independiente-noche.png" alt="La temperatura de la calefacción de la casa distinta para cada habitación"></div>
                <div><img class="img-responsive padding-t-30" src="<?php echo get_template_directory_uri(); ?>/img/calefaccion-control-independiente-dormitorios.png" alt="La temperatura de la calefacción de la casa distinta para cada habitación"></div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12 offset-lg-2 col-lg-8">
              <span class="display-b text-align-c">
                <a class="btn-s-b" href="<?php echo get_site_url(); ?>/informacion-tecnica">Información técnica</a>
              </span>
            </div>
          </div>

        </div>
      </article>
      <!-- FIN article 02 -->

      
      <!-- article 03 -->
      <article class="articulo padding-b-50">
        <div class="row">
          <header class="art-cabecera">
            <div class="col-md-12">
              <h2 class="art-num num-03 font-size-xl font-weight-l text-align-c color-w-0100">Compacto de silicio</h2>
            </div>
          </header>
        </div>
        <!-- art-division -->
        <div class="art-cuerpo">

          <div class="row">
            <div class="col-md-12">
              <div class="text-align-c margin-b-8">
                <img class="display-b width-100 margin-0-auto" src="<?php echo get_template_directory_uri(); ?>/img/maxima-dureza-resistencia-sello.png" alt="Sello certificado Bureau Veritas">
              </div>
              <p class="font-size-xs font-weight-n text-align-c color-w-0100 margin-b-18"><em class="c-secondary">9 en la Escala de Mohs</em></p>
            </div>

            <div class="col-md-6">
              <div class="owl-carousel">
                  <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/maxima-dureza-compacto-silicio-acumulacion.jpg" alt="Fabricación compacto de silicio máxima resistencia"></div>
                  <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/maxima-dureza-compacto-silicio-fabricacion.jpg" alt="Fabricación compacto de silicio máxima resistencia"></div>
                  <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/maxima-dureza-compacto-silicio-resistencia.jpg" alt="Fabricación compacto de silicio máxima resistencia"></div>
                  <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/compacto-silicio-silex-perla.jpg" alt="Compacto silicio silex perla"></div>
                  <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/compacto-silicio-silex-perla-cocina.jpg" alt="Compacto silicio silex perla cocina"></div>
                  <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/compacto-silicio-silex-dark-cocina.jpg" alt="Compacto silicio silex dark cocina"></div>
              </div>
            </div>
            <div class="col-md-6">
              <p class="font-size-s font-weight-n text-align-l color-w-0100 luto-izq">Nuestros equipos Newport incorporan un compactado compuesto fundamentalmente de <strong class="c-secondary">óxido de silicio (gran acumulador térmico)</strong> y <strong class="c-secondary">óxido de aluminio (gran transmisor)</strong>. De esta combinación surge un material ideal para su aplicación en la calefacción, por su capacidad de <strong class="c-secondary">acumulación y transmisión</strong>.</p>
              <img class="img-responsive padding-t-30 padding-r-50 padding-b-30 padding-l-50" src="<?php echo get_template_directory_uri(); ?>/img/silicio-y-aluminio.png" alt="El silicio acumula y el aluminio transmite">
              <span class="display-b text-align-c">
                <a class="btn-s-b" href="<?php echo get_site_url(); ?>/informacion-tecnica">Información técnica</a>
              </span>
            </div>
          </div>
        </div>
      </article>
      <!-- FIN article 03 -->


      <!-- article 04 -->
      <article class="articulo padding-b-50">
        <div class="row">
          <header class="art-cabecera">
            <div class="col-md-12">
              <h2 class="art-num num-04 font-size-xl font-weight-l text-align-c color-w-0100">Termostatos</h2>
            </div>
          </header>
        </div>
        <!-- art-division -->
        <div class="art-cuerpo">
          <div class="row">
            <div class="col-md-6">
              <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">Los radiadores Newport cuentan con <strong class="c-secondary">termostatos de última generación</strong>, de gran sensibilidad, que medirán la temperatura de un modo exacto.</p>
              <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18 luto-izq">El cuadro de mandos de nuestros termostatos está situado en la parte superior del equipo para su facilidad de uso. Sin embargo, nuestra <strong class="c-secondary">botonera</strong> es <strong class="c-secondary">abatible</strong>, de modo que cuando no necesite utilizarla, puede ocultarla a la vista. Se consigue así que el equipo <strong class="c-secondary">respete totalmente la estética de su habitación</strong>.</p>
            </div>
            <div class="col-md-6">
              <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/termostato-wifi-ultima-generacion-botonera-abatible.jpg" alt="Termostato de última generación con control wifi y botonera abatible">
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 offset-lg-2 col-lg-8">
              <span class="display-b text-align-c margin-t-18">
                <a class="btn-s-b" href="<?php echo get_site_url(); ?>/informacion-tecnica">Información técnica</a>
              </span>
            </div>
          </div>
        </div>
      </article>
      <!-- FIN article 04 -->


      <!-- article 05 -->
      <article class="articulo padding-b-50">
        <div class="row">
          <header class="art-cabecera">
            <div class="col-md-12">
              <h2 class="art-num num-05 font-size-xl font-weight-l text-align-c color-w-0100">Consumo energético</h2>
            </div>
          </header>
        </div>
        <!-- art-division -->
        <div class="art-cuerpo">
          <div class="row">
            <div class="col-md-12 col-lg-6">
              <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">Gracias a todas las anteriores características <strong class="c-secondary">el ahorro es considerable</strong>.</p>
              <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">Recientemente la prestigiosa BRE del Reino Unido <em class="c-secondary">(Building Research Establishment)</em> ha realizado un test comparando nuestro sistema con algunos de los más populares, como acumuladores  nocturnos, gas o radiadores eléctricos tradicionales. Los resultados hablan por sí solos: <strong class="c-secondary">El radiador Newport consume 1,16 kw en una hora. Un 67 % menos de consumo que otros equipos</strong>.</p>
              <span class="luto-izq margin-b-30 display-b width-0100">
                <a class="btn-s-b" target="_blank" href="<?php echo get_template_directory_uri(); ?>/test-report-bre-estudio.pdf">Descargar estudio completo</a>
              </span>
            </div>
            <div class="offset-md-4 col-md-8 offset-lg-0 col-lg-6">
              <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/grafica-kw-consumidos.png" alt="Gráfica ahorro kw consumidos">
            </div>
          </div>
        </div>
      </article>
      <!-- FIN article 05 -->

  </div>
  <!-- FIN container -->
</section>
<!-- FIN cuerpo -->

<aside class="testimonios">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      </div>
    </div>
  </div>
  <!-- FIN container -->
</aside>
<!-- FIN testimonios -->


<?php // get_sidebar(); ?>

<?php get_footer(); ?>
