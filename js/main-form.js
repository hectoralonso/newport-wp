$(document).ready(function(e){	


	

	/* fomulario de contacto */
	$.validator.setDefaults({
		submitHandler: function() {
			$.ajax({
				data: $("#form-contacto").serialize(),
				url: 'ajax/ajax_envia_consulta_contacto.php',
				type: 'post',			
					success: function(){
					$("#form-contacto").html('<p class="text-align-c color-w-0100">Hemos recibido su consulta. En breve nos pondremos en contacto usted. Un saludo.</p>');
				  }
				});
		}
	});	
	$("#form-contacto").validate({
		rules: {
			nombre_contacto: {
				required: true
			},
			email_contacto: {
				required: true,
				email: true
			},
			tel_contacto: {
				required: true
			},
			direccion_contacto: {
				required: true
			},
			ciudad_contacto: {
				required: true
			},
			cp_contacto: {
				required: true
			},
			consulta_contacto: {
				required: true
			}
		},
		messages: {
			nombre_contacto: {
				required: "Rellene este campo con su nombre"
			},
			email_contacto: {
				required: "Rellene este campo con su e-mail",
				email: "Rellene este campo con un e-mail válido"
			},
			tel_contacto: {
				required: "Rellene este campo con su teléfono"
			},
			direccion_contacto: {
				required: "Rellene este campo con su dirección"
			},
			ciudad_contacto: {
				required: "Rellene este campo con su ciudad"
			},
			cp_contacto: {
				required: "Rellene este campo con su código postal"
			},
			consulta_contacto: {
				required: "Rellene este campo con el motivo de su consulta"
			}
		}
	});
	/* FIN fomulario de contacto */


	/* fomulario de estudio gratuito */
	$.validator.setDefaults({
		submitHandler: function() {
			$.ajax({
				data: $("#form-estudio").serialize(),
				url: 'ajax/ajax_envia_estudio.php',
				type: 'post',			
					success: function(){
					$("#form-estudio").html('<p class="text-align-c color-w-0100">Hemos recibido su consulta. En breve nos pondremos en contacto usted. Un saludo.</p>');
				  }
				});
		}
	});	
	$("#form-estudio").validate({
		rules: {
			nombre_contacto: {
				required: true
			},
			email_contacto: {
				required: true,
				email: true
			},
			tel_contacto: {
				required: true
			},
			ciudad_contacto: {
				required: true
			}
		},
		messages: {
			nombre_contacto: {
				required: "Rellene este campo con su nombre"
			},
			email_contacto: {
				required: "Rellene este campo con su e-mail",
				email: "Rellene este campo con un e-mail válido"
			},
			tel_contacto: {
				required: "Rellene este campo con su teléfono"
			},
			ciudad_contacto: {
				required: "Indíquenos el motivo de su consulta"
			}
		}
	});
	/* FIN fomulario de estudio gratuito */


});