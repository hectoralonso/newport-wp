$(document).ready(function(e){	

	var win_width = $(window).width();
	var win_height = $(window).height();
	var cabecera_height = $("#cabecera").height();
	//$('.diapos').length;
	var num_diapos = 4;
	var cabecera_height_entre_diapos = cabecera_height/4;


	
	//$("#tel-gratis").html(cabecera_height_entre_diapos);	


	/* cabecera */
	/* ************ posibilidad de meter un if para apaisado ************* */
	//if ( win_width >= 992){

		// if (cabecera_height > win_width){
		// 	$(".alto-ventana").css("height", cabecera_height_entre_diapos+"px");	    
		// }
		// else{
		// 	$(".alto-ventana").css("height", win_height+"px");	   
		// }

		$(".alto-ventana").css("height", win_height+"px");	
		$(window).resize(function() {
			var re_win_height = $(window).height();
	        $(".alto-ventana").css("height", re_win_height+"px");	
		});

	//}
	/* FIN cabecera */


	/* simplelightbox */
	$('.imageGallery1 a').simpleLightbox();
	

	// setTimeout(function() {
	//     $('#btn-tecnico').trigger('click');
	// }, 10000);


	// function randomImage() {
	// 	if ( win_width >= 992){
	// 		var fileNames = [
	// 		    "img/estanteria-silex-nature-vertical.jpg",
	// 		    "img/estanteria-silex-nature.jpg",
	// 		    "img/estanteria-silex-perla-vertical.jpg",
	// 		    "img/estanteria-silex-perla.jpg",
	// 		    "img/estanteria-silex-slate-vertical.jpg",
	// 		    "img/estanteria-silex-dark.jpg"
	// 		];
	// 	}
	// 	else{
	// 		var fileNames = [
	// 		    "img/estanteria-silex-nature-vertical-xs.jpg",
	// 		    "img/estanteria-silex-nature-xs.jpg",
	// 		    "img/estanteria-silex-perla-vertical-xs.jpg",
	// 		    "img/estanteria-silex-perla-xs.jpg",
	// 		    "img/estanteria-silex-slate-vertical-xs.jpg",
	// 		    "img/estanteria-silex-dark-xs.jpg"
	// 		];
	// 	}
	// 	var randomIndex = Math.floor(Math.random() * fileNames.length);
	// 	//document.getElementById("cabecera").style.background = 'url(' + fileNames[randomIndex] + ')';
	// 	$(".estanteria-carrusel").css("background-image", 'url(' + fileNames[randomIndex] + ')');
	// }
	// function getRandomTime() {
	// 	return 3000;
	// 	// return Math.round(Math.random() * 5000);
	// }
	// (function loop() {
	// 	setTimeout(function() {
	// 	randomImage();
	// 	loop();
	// 	}, getRandomTime());
	// })();


	/* menu movil */
	$(".menu-movil").css("height", win_height+"px");  
	$(window).resize(function() {
		var re_win_height = $(window).height();
        $(".menu-movil").css("height", re_win_height+"px");	
	});
    $("#menu-movil").css("display", "none");
	$("#cabecera").on("click","#menu-hamburguesa",function(e){
		e.preventDefault();		
		if ($("#menu-movil").is(":visible")) {
			$("#menu-movil").fadeOut("slow",function(){				 
			});
		}else{
			$("#menu-movil").fadeIn("slow",function(){
			});			 
		}
	});
	$("#menu-movil").on("click","#cerrar-menu-movil",function(e){
		e.preventDefault();	
		if ($("#menu-movil").is(":visible")) {
			$("#menu-movil").fadeOut("slow",function(){				 
			});
		}else{
			$("#menu-movil").fadeIn("slow",function(){
			});			 
		}		 
	});
	/* FIN menu movil */


	/* slide estudio */
	$("#btn-estudio-gratis").on('click', function(){
    	$('#estudio-gratis').toggleClass('muestra');
	});

	$("#aspa-estudio").on('click', function(){
    	$('#estudio-gratis').toggleClass('muestra');
	});
	/* FIN slide estudio */
	


	/* scroll animado */
	$("#cabecera").on("click","#btn-cuerpo",function(e){
		e.preventDefault();
		var desti = $("#cuerpo").offset().top;
		$("html, body").animate({scrollTop: desti}); 
	});
	$("#cabecera").on("click","#btn-control-independiente",function(e){
		e.preventDefault();
		var desti = $("#control-independiente").offset().top;
		$("html, body").animate({scrollTop: desti}); 
	});
	$("#cabecera").on("click","#btn-termostatos",function(e){
		e.preventDefault();
		var desti = $("#termostatos").offset().top;
		$("html, body").animate({scrollTop: desti}); 
	});
	$("#cabecera").on("click","#btn-wifi",function(e){
		e.preventDefault();
		var desti = $("#wifi").offset().top;
		$("html, body").animate({scrollTop: desti}); 
	});
	$("#cabecera").on("click","#btn-consumo-energetico",function(e){
		e.preventDefault();
		var desti = $("#consumo-energetico").offset().top;
		$("html, body").animate({scrollTop: desti}); 
	});
	$("#cabecera").on("click","#btn-compacto-silicio",function(e){
		e.preventDefault();
		var desti = $("#compacto-silicio").offset().top;
		$("html, body").animate({scrollTop: desti}); 
	});
	/* FIN scroll animado */
	



});