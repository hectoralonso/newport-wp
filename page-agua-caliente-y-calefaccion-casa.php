<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header("agua-caliente-y-calefaccion-casa"); ?>

<section id="cuerpo" class="bg-color-p-0100">
      <div class="container">

          <!-- article 01 -->
          <article class="articulo padding-b-50">
            <div class="row">
              <header class="art-cabecera">
                <div class="col-md-12">
                  <h2 class="art-num num-01 font-size-xl font-weight-l text-align-c color-w-0100">Energía solar para su agua caliente</h2>
                </div>
              </header>
            </div>
            <!-- art-division -->
            <div class="art-cuerpo">
              <div class="row">
                <div class="col-md-12 offset-lg-2 col-lg-8">
                  <p class="font-size-s font-weight-n text-align-c color-w-0100 margin-b-30"><strong class="c-secondary">La energía solar es, sin duda, uno de los sistemas de generación de agua caliente más eficientes y económicos</strong>. Hasta ahora las limitaciones de este sistema eran la necesidad de contar con un depósito voluminoso, que garantizase el caudal de agua caliente que necesita una familia y la lenta recuperación en caso de agotar ese depósito, necesitando varias horas para volver a contar con agua caliente.</p>
                </div>
                <div class="col-md-12">
                  <div class="art-cuerpo">
                    <div class="row">
                      <div class="col-md-3">
                        <!-- <h3 class="font-size-m font-weight-b text-align-l color-w-0100 margin-b-10">Lorem ipsum</h3> -->
                        <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18 luto-izq">Ahora con nuestra <strong class="c-secondary">energía solar</strong>, a través de la combinación con nuestro <strong class="c-secondary">termo wifi</strong>, estos inconvenientes prácticamente desaparecen.</p>
                      </div>
                      <div class="col-md-6">
                        <img class="img-responsive margin-b-18" src="<?php echo get_template_directory_uri(); ?>/img/aerotermia-agua-caliente-eficaz-economico.jpg" alt="Ducha de agua caliente con aerotermia y wifi">
                      </div>
                      <div class="col-md-3">
                        <!-- <h3 class="font-size-m font-weight-b text-align-l color-w-0100 margin-b-10">Lorem ipsum</h3> -->
                        <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18 luto-izq">Nuestro sistema ha demostrado que puede suministrar <strong class="c-secondary">agua caliente ininterrumpidamente durante más de una hora</strong>, con el grifo abierto al máximo… ¿Se imagina?</p>
                      </div>
                      <div class="offset-md-4 col-md-3">
                        <!-- <h3 class="font-size-m font-weight-b text-align-l color-w-0100 margin-t-20 margin-b-10">Lorem ipsum</h3> -->
                        <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18 luto-izq">Es <strong class="c-secondary">la solución más eficiente</strong>, que garantiza a la vez agua caliente prácticamente ilimitada para un uso de familia numerosa.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </article>
          <!-- FIN article 01 -->

          <!-- article 02 -->
          <article class="articulo padding-b-50">
            <div class="row">
              <header class="art-cabecera">
                <div class="col-md-12">
                  <h2 class="art-num num-02 font-size-xl font-weight-l text-align-c color-w-0100">Termo wifi</h2>
                </div>
              </header>
            </div>
            <!-- art-division -->
            <div class="art-cuerpo">
              <div class="row">
                <div class="col-md-6">
                  <img class="img-responsive margin-b-30" src="<?php echo get_template_directory_uri(); ?>/img/termo-wifi-armario-cocina-naomi-hebert.jpg" alt="termo wifi armario cocina copyright naomi-hebert">
                </div>
                <div class="col-md-6">
                  <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">Nuestro <strong class="c-secondary">termo wifi</strong>, con un doble depósito y un diseño pensado para integrarse en cualquier armario de cocina estándar, permite un control de la potencia a consumir, integrándose en la gestión de la calefacción de su hogar, maximizando su control y permitiendo a nuestros clientes vigilar sus consumos.</p>
                  <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">La energía solar y el termo wifi interactúan a través de una <strong class="c-secondary">válvula termostática</strong>, encargada de controlar en cada momento la temperatura del agua a suministrar y discriminando, en función de las necesidades, el suministro de la misma, procedente de la energía solar o del termo. Todo ello contando con un <strong class="c-secondary">sistema de descalcificación</strong> y <strong class="c-secondary">control del caudal</strong>.</p>
                </div>
              </div>
            </div>
          </article>
          <!-- FIN article 02 -->

          <!-- article 03 -->
          <article class="articulo padding-b-50">
            <div class="row">
              <header class="art-cabecera">
                <div class="col-md-12">
                  <h2 class="art-num num-03 font-size-xl font-weight-l text-align-c color-w-0100">Calefacción por agua caliente</h2>
                </div>
              </header>
            </div>
            <!-- art-division -->
            <div class="art-cuerpo">
              <div class="row">
                <div class="col-md-12 offset-lg-2 col-lg-8">
                  <p class="font-size-s font-weight-n text-align-c color-w-0100 margin-b-30">También disponemos de <strong class="c-secondary">radiadores de agua</strong>, con nuestro compacto de silicio, que pueden funcionar con <strong class="c-secondary">energía solar</strong> o cualquier otro sistema de calefacción mediante agua, ocultando los tradicionales radiadores bajo nuestro <strong class="c-secondary">estético compacto</strong>.</p>
                </div>
                <div class="col-md-12">
                  <div class="art-cuerpo">
                    <div class="row">
                      <div class="offset-md-3 col-md-6">
                        <div class="owl-carousel">
                          <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/radiador-agua-h2o-pizarra-azabache.jpg" alt="Radiador agua H2O pizarra"></div>
                          <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/radiador-agua-h2o-pizarra-nieve-cuadrado.jpg" alt="Radiador agua H2O pizarra"></div>
                          <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/radiador-agua-h2o-pizarra-azabache-vertical.jpg" alt="Radiador agua H2O pizarra"></div>
                          <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/radiador-agua-h2o-roble-claro-horizontal.jpg" alt="Radiador agua H2O roble"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </article>
          <!-- FIN article 03 -->

      </div>
      <!-- FIN container -->
    </section>
    <!-- FIN cuerpo -->

<?php // get_sidebar(); ?>
<?php get_footer(); ?>