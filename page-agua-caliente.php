<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header("agua-caliente"); ?>

<section id="cuerpo" class="bg-color-p-0100">
  <div class="container">

      <!-- article 01 -->
      <article class="articulo padding-b-50">
        <div class="row">
          <header class="art-cabecera">
            <div class="col-md-12">
              <h2 class="art-num num-01 font-size-xl font-weight-l text-align-c color-w-0100">¿Cómo es posible?</h2>
            </div>
          </header>
        </div>
        <!-- art-division -->
        <div class="art-cuerpo">
          <div class="row">
            <div class="col-md-12 offset-lg-2 col-lg-8">
              <p class="font-size-s font-weight-n text-align-c color-w-0100 margin-b-20">Utilizando sistemas multitanque de reducido espacio, creamos una serie de circuitos asistidos por aporte de calor múltiple e independiente, súper aislados individualmente y controlado por un sistema wifi único, que evita los excesos de consumo y optimiza al 100% la potencia disponible en el hogar en cada momento. Así,  nuestro termo utiliza sólo aquella energía libre o más barata de la que pueda disponer, ya sea por las tarifas nocturnas, la aerotermia o la energía solar, asegurando que nunca se quedará sin agua y que el consumo será el más económico posible. Todo ello de forma 100% automática. <strong class="c-secondary">Con Newport, cero problemas</strong>.</p>
            </div>
           </div>
           <div class="row">
              <div class="offset-md-2 col-md-8 offset-lg-4 col-lg-4">
                  <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/aerotermia-agua-caliente-eficaz-economico.jpg" alt="Ducha de agua caliente con aerotermia y wifi">
                </div>
            </div>
        </div>
      </article>
      <!-- FIN article 01 -->


      <!-- article 02 -->
      <article class="articulo padding-b-50">
        <div class="row">
          <header class="art-cabecera">
            <div class="col-md-12">
              <h2 class="art-num num-02 font-size-xl font-weight-l text-align-c color-w-0100">¿Es muy complejo instalar Newport?</h2>
            </div>
          </header>
        </div>
        <!-- art-division -->
        <div class="art-cuerpo">
          <div class="row">
            <div class="col-md-12 offset-lg-2 col-lg-8">
              <p class="font-size-s font-weight-n text-align-c color-w-0100 margin-b-20">No, para usted no lo es. Nuestro sistema sólo puede ser instalado por expertos que aseguren nuestros estándares de calidad, por lo tanto, sólo será instalado por nuestros profesionales. Nos adaptaremos a su hogar y le propondremos las diferentes combinaciones que mejor se adapten al uso de su familia asegurándole el mejor precio y la instalación más sencilla. El sistema es 100% automático, nuestra asistencia es inmediata y continua, tanto en persona como por teléfono. Lo haremos de forma profesional y sin obras. <strong class="c-secondary">Con Newport, cero problemas</strong>.</p>
            </div>
           </div>
           <div class="row">
              <div class="offset-md-2 col-md-8 offset-lg-4 col-lg-4">
                  <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/instalador-newport.jpg" alt="instalado por expertos que aseguren nuestros estándares de calidad">
                </div>
            </div>
        </div>
      </article>
      <!-- FIN article 02 -->

      
      <!-- article 03 -->
      <article class="articulo padding-b-50">
        <div class="row">
          <header class="art-cabecera">
            <div class="col-md-12">
              <h2 class="art-num num-03 font-size-xl font-weight-l text-align-c color-w-0100">¿Es muy caro?</h2>
            </div>
          </header>
        </div>
        <!-- art-division -->
        <div class="art-cuerpo">
          <div class="row">
            <div class="col-md-12 offset-lg-2 col-lg-8">
              <p class="font-size-s font-weight-n text-align-c color-w-0100 margin-b-20">Le sorprenderá nuestros precios. Nuestros técnicos estudiarán su caso y se adaptarán a su uso y presupuesto. Tenemos soluciones para todos los planteamientos, desde pequeños apartamentos a grandes viviendas unifamiliares. Además trabajamos con diversas entidades financieras que le permitirán afrontar los pagos de la forma que más le convenga y en plazos que se adaptan a su capacidad. <strong class="c-secondary">Con Newport, cero problemas</strong>.</p>
            </div>
           </div>
           <div class="row">
              <div class="offset-md-2 col-md-8 offset-lg-4 col-lg-4">
                  <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/agua-caliente-competitiva.jpg" alt="precios competitivos">
                </div>
            </div>
        </div>
        </article>
      <!-- FIN article 03 -->


      <!-- article 04 -->
      <article class="articulo padding-b-50">
        <div class="row">
          <header class="art-cabecera">
            <div class="col-md-12">
              <h2 class="art-num num-04 font-size-xl font-weight-l text-align-c color-w-0100">¿Es ruidoso?</h2>
            </div>
          </header>
        </div>
        <!-- art-division -->
        <div class="art-cuerpo">
          <div class="row">
            <div class="col-md-12 offset-lg-2 col-lg-8">
              <p class="font-size-s font-weight-n text-align-c color-w-0100 margin-b-20">Cero ruidos, nuestro sistema trabaja a 20 decibelios. Es absolutamente silencioso. Le recordamos que otros sistemas del mercado emiten hasta 50 decibelios. <strong class="c-secondary">Con Newport, cero problemas</strong>.</p>
            </div>
           </div>
            <div class="row">
              <div class="offset-md-2 col-md-8 offset-lg-4 col-lg-4">
                  <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/agua-caliente-sin-ruidos-silencioso.jpg" alt="silencioso"></div>
              </div>
          </div>
      </article>
      <!-- FIN article 04 -->

  </div>
  <!-- FIN container -->
</section>
<!-- FIN cuerpo -->

<?php // get_sidebar(); ?>
<?php get_footer(); ?>
