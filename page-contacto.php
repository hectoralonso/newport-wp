<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header("contacto"); ?>

<section id="cuerpo" class="bg-color-p-0100">
  <div class="container">
    <div class="row">
      <form id="form-contacto" class="formulario overflow-h padding-t-50 padding-b-50" method="post" role="form">
        <div class="col-md-6">
          <div class="form-group padding-t-10 padding-b-10">
            <label class="color-w-0100 display-b font-size-s margin-b-4" for="nombre_contacto">Nombre</label>
            <input type="text" class="form-control width-0100 font-size-s" id="nombre_contacto" name="nombre_contacto" value="" required>
          </div>
          <div class="form-group padding-t-10 padding-b-10">
            <label class="color-w-0100 display-b font-size-s margin-b-4" for="email_contacto">Email</label>
            <input type="email" class="form-control width-0100 font-size-s" id="email_contacto" name="email_contacto" value="" required>
          </div>
          <div class="form-group padding-t-10 padding-b-10">
            <label class="color-w-0100 display-b font-size-s margin-b-4" for="ciudad_contacto">Ciudad</label>
            <input type="text" class="form-control width-0100 font-size-s" id="ciudad_contacto" name="ciudad_contacto" value="" required>
          </div>
          <div class="form-group padding-t-10 padding-b-10">
            <label class="color-w-0100 display-b font-size-s margin-b-4" for="como_nos_encontro">¿Cómo nos encontró?</label>
            <select class="form-control width-0100 font-size-s padding-t-7 padding-b-6" id="como_nos_encontro" name="como_nos_encontro">
              <option value="anuncio-prensa">Anuncio en prensa</option>
              <option value="buscador">Búsqueda en internet</option>
              <option value="anuncio-net">Anuncio en internet</option>
              <option value="recomendacion">Recomendación de un conocido</option>
              <option value="folleto">Folleto en su buzón</option>
              <option value="radio">Radio</option>
            </select>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group padding-t-10 padding-b-10">
            <label class="color-w-0100 display-b font-size-s margin-b-4" for="tel_contacto">Teléfono</label>
            <input type="text" class="form-control width-0100 font-size-s" id="tel_contacto" name="tel_contacto" value="" required>
          </div>
          <div class="form-group padding-t-10 padding-b-10">
            <label class="color-w-0100 display-b font-size-s margin-b-4" for="direccion_contacto">Dirección</label>
            <input type="text" class="form-control width-0100 font-size-s" id="direccion_contacto" name="direccion_contacto" value="" required>
          </div>
          <div class="form-group padding-t-10 padding-b-10">
            <label class="color-w-0100 display-b font-size-s margin-b-4" for="cp_contacto">Código postal</label>
            <input type="text" class="form-control width-0100 font-size-s" id="cp_contacto" name="cp_contacto" value="" required>
          </div>
          <div class="form-group padding-t-10 padding-b-18 color-w-0100">
            <span class="color-w-0100 display-b font-size-s margin-b-10">¿Está interesado en?</span>
            <div class="form-check form-check-inline display-i">
              <label for="estudio" class="form-check-label font-size-s">
                <input class="form-check-input" type="checkbox" id="estudio" name="estudio" value="estudio"> Estudio gratuito
              </label>
            </div>
            <div class="form-check form-check-inline display-i">
              <label for="catalogo" class="form-check-label font-size-s">
                <input class="form-check-input" type="checkbox" id="catalogo" name="catalogo" value="catalogo"> Catálogo gratuito
              </label>
            </div>
            <div class="form-check form-check-inline display-i margin-l-5">
              <label for="llamada" class="form-check-label font-size-s">
                <input class="form-check-input" type="checkbox" id="llamada" name="llamada" value="llamada"> Llamada informativa
              </label>
            </div>
            <div class="form-check form-check-inline display-i margin-l-5">
              <label for="informacion" class="form-check-label font-size-s">
                <input class="form-check-input" type="checkbox" id="informacion" name="informacion" value="informacion"> Más información
              </label>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group padding-t-10 padding-b-10">
            <label class="color-w-0100 display-b font-size-s margin-b-4" for="consulta_contacto">Consulta</label>
            <textarea class="form-control width-0100 font-size-s" rows="4" id="consulta_contacto" name="consulta_contacto" required></textarea>
          </div>
          <div class="form-group padding-t-10 padding-b-10">
            <span class="display-b text-align-r">
              <button type="submit" id="enviar_contacto" class="btn-s-b border-6" name="enviar_contacto">Enviar</button>
            </span>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!-- FIN container -->
</section>
<!-- FIN cuerpo -->

<?php // get_sidebar(); ?>
<?php get_footer("contacto"); ?>
