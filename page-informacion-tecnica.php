<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header("informacion-tecnica"); ?>

<section id="cuerpo" class="bg-color-p-0100">
 <div class="container">

  <!-- article 01 -->
  <article id="control-independiente" class="articulo padding-b-50">
    <div class="row">
      <header class="art-cabecera">
        <div class="col-md-12">
          <h2 class="art-num num-01 font-size-xl font-weight-l text-align-c color-w-0100">Control independiente por habitación</h2>
        </div>
      </header>
    </div>
    <!-- art-division -->
    <div class="art-cuerpo">
      <div class="row">
        <div class="offset-md-2 col-md-8">
          <p class="font-size-s font-weight-n text-align-c color-w-0100 margin-b-18"><strong class="c-secondary">¿Por qué calentar toda la casa a la misma temperatura?</strong></p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">Nuestras necesidades de calefacción son muy distintas dependiendo de cada habitación. Por qué calentar el dormitorio a 21 grados todo el día si verdaderamente sólo lo utilizamos durante la noche. Con los radiadores Newport podemos programar cada habitación de la casa de un modo distinto, estableciendo la temperatura que queremos en cada momento y día de la semana y de un modo intuitivo y fácil.</p>
          <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">Además, a diferencia de otros sistemas, que generan el calor en una caldera y pierden gran parte de ese calor en el trayecto hasta la habitación -como sucede con las caldera de gas, con pérdidas en la mayoría de los casos de más de un 30 % de calor en el trayecto que recorre desde la caldera al radiador- con Newport el calor se produce directamente en la habitación que queremos, evitando así las pérdidas.</p>
        </div>
        <div class="col-md-6">
          <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">Además es calefacción eléctrica, no quema combustibles fósiles ni genera gases contaminantes. El futuro de la energía es eléctrico, ligado a energías renovables y ecológicas. Cada vez hay más coches eléctricos y lo mismo sucederá con los sistemas de calefacción, que serán eléctricos, eficientes y ecológicos.</p>
          <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">Con Newport usted tendrá todo el control de su calefacción, en cada habitación y en cada momento, aunque esté de viaje, gracias a nuestro sistema wifi. ¡Tome el control y mejore su eficiencia y sus facturas!</p>
        </div>
      </div>
      <div class="row">
          <div class="col-md-12 offset-lg-2 col-lg-8">
            <div class="owl-carousel owl-theme owl-horas">
              <div><img class="img-responsive padding-t-30" src="<?php echo get_template_directory_uri(); ?>/img/calefaccion-control-independiente-desayuno.png" alt="La temperatura de la calefacción de la casa distinta para cada habitación"></div>
              <div><img class="img-responsive padding-t-30" src="<?php echo get_template_directory_uri(); ?>/img/calefaccion-control-independiente-vacia.png" alt="La temperatura de la calefacción de la casa distinta para cada habitación"></div>
              <div><img class="img-responsive padding-t-30" src="<?php echo get_template_directory_uri(); ?>/img/calefaccion-control-independiente-tarde.png" alt="La temperatura de la calefacción de la casa distinta para cada habitación"></div>
              <div><img class="img-responsive padding-t-30" src="<?php echo get_template_directory_uri(); ?>/img/calefaccion-control-independiente-cena.png" alt="La temperatura de la calefacción de la casa distinta para cada habitación"></div>
              <div><img class="img-responsive padding-t-30" src="<?php echo get_template_directory_uri(); ?>/img/calefaccion-control-independiente-noche.png" alt="La temperatura de la calefacción de la casa distinta para cada habitación"></div>
              <div><img class="img-responsive padding-t-30" src="<?php echo get_template_directory_uri(); ?>/img/calefaccion-control-independiente-dormitorios.png" alt="La temperatura de la calefacción de la casa distinta para cada habitación"></div>
          </div>
        </div>
      </div>
    </div>
  </article>
  <!-- FIN article 01 -->

  <!-- article 02 -->
  <article id="termostatos" class="articulo padding-b-50">
    <div class="row">
      <header class="art-cabecera">
        <div class="col-md-12">
          <h2 class="art-num num-02 font-size-xl font-weight-l text-align-c color-w-0100">Termostatos</h2>
        </div>
      </header>
    </div>
    <!-- art-division -->
    <div class="art-cuerpo">
      <div class="row">
        <div class="margin-b-40 overflow-h">
          <div class="col-md-6">
            <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">Los radiadores Newport cuentan con termostatos de última generación, de gran sensibilidad, colocados en la parte baja del equipo, que medirán la temperatura de un modo exacto. Activan el radiador únicamente cuando la temperatura descienda por debajo de lo que usted ha prescrito.</p>
          </div>
          <div class="col-md-6">
            <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">El cuadro de mandos de nuestros termostatos está situado en la parte superior del equipo, para su facilidad de uso. Sin embargo, nuestra botonera es abatible, de modo que cuando no necesite utilizarla, puede ocultarla a la vista, haciendo que el equipo respete totalmente la estética de su habitación.</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="art-cuerpo">
            <div class="row"> 
              <div class="offset-md-3 col-md-6">
                <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/termostato-wifi-ultima-generacion-botonera-abatible.jpg" alt="Termostato de última generación con control wifi y botonera abatible">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </article>
  <!-- FIN article 02 -->

  <!-- article 03 -->
  <article id="wifi" class="articulo padding-b-50">
    <div class="row">
      <header class="art-cabecera">
        <div class="col-md-12">
          <h2 class="art-num num-03 font-size-xl font-weight-l text-align-c color-w-0100">Wifi</h2>
        </div>
      </header>
    </div>
    <!-- art-division -->
    <div class="art-cuerpo">
      <div class="row">
        <div class="offset-md-2 col-md-8">
          <p class="font-size-s font-weight-n text-align-c color-w-0100 margin-b-18"><strong class="c-secondary">Nuestros radiadores son controlables mediante nuestro sistema wifi, lo que permite:</strong></p>
        </div>
      </div>
      <div class="row">
        <div class="margin-b-40 overflow-h">
          <div class="col-md-6">
            <ul>
              <li class="padding-l-30 bolo-listado font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">Personalizar la temperatura de cada habitación en función de su uso, dimensiones y horario desde cualquier tablet o smartphone.</li>
              <li class="padding-l-30 bolo-listado font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">Asignar la cantidad de energía a utilizar por día, estableciendo prioridades, de modo que los equipos consumirán la energía fijada discriminando las prioridades que usted le asigne, incluyendo el consumo de agua caliente. Si, por ejemplo, usted da prioridad a su salón, esa habitación mantendrá el calor que desea, aún cuando el consumo se acerque al máximo de energía que ha fijado. El sistema disminuirá el consumo de los equipos situados en otras habitaciones no prioritarias. ¡Evite sustos en su factura de energía!</li>
            </ul>
          </div>
          <div class="col-md-6">
            <ul>
              <li class="padding-l-30 bolo-listado font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">Localización geográfica. A través de su smartphone nuestro sistema detecta su proximidad al hogar, activando las temperaturas de confort previamente, del modo más eficiente.</li>
              <li class="padding-l-30 bolo-listado font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">Predictivo. El sistema registra sus hábitos de uso, adaptando su funcionamiento programado del modo más óptimo posible y totalmente de forma automática.</li>
              <li class="padding-l-30 bolo-listado font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">Detección ventanas abiertas. Ante una caída brusca de la temperatura los equipos automáticamente cortan el consumo, ahorrando dinero cada vez que ventilamos la casa.</li>
            </ul>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="art-cuerpo">
          <div class="col-md-12 offset-lg-3 col-lg-6">
            <div class="owl-carousel">
                <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/wifi-calefaccion-salon.jpg" alt="Sistema wifi"></div>
                <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/wifi-calefaccion-habitacion.jpg" alt="Sistema wifi"></div>
                <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/toallero-wifi.jpg" alt="Sistema wifi"></div>
            </div>
          </div>
        </div>
    </div>
  </div>
  </article>
  <!-- FIN article 03 -->

  <!-- article 04 -->
  <article id="consumo-energetico" class="articulo padding-b-50">
    <div class="row">
      <header class="art-cabecera">
        <div class="col-md-12">
          <h2 class="art-num num-04 font-size-xl font-weight-l text-align-c color-w-0100">Consumo energético</h2>
        </div>
      </header>
    </div>
    <!-- art-division -->
    <div class="art-cuerpo">
      <div class="row">
        <div class="col-md-12 offset-lg-2 col-lg-8">
          <p class="font-size-s font-weight-n text-align-c color-w-0100 margin-b-18"><strong class="c-secondary">Newport es uno de los sistemas más eficientes en el mercado. Nuestra calefacción logra unos ahorros considerables gracias a:</strong></p>
        </div>
      </div>
      <div class="row">
        <div class="margin-b-40 overflow-h">
          <div class="col-md-6">
            <ul>
              <li class="padding-l-30 bolo-listado font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">Nuestro compacto de silicio, que transmite un calor uniforme de un modo rápido y constante.</li>
              <li class="padding-l-30 bolo-listado font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">La incorporación de termostatos de control de temperatura de última generación, de alta sensibilidad, que evita más temperatura de la deseada en la habitación y, por lo tanto, más consumo.</li>
              <li class="padding-l-30 bolo-listado font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">Nuestra central wifi, le permite incluso asignar la cantidad de energía a utilizar por día, estableciendo prioridades, de modo que los equipos consumirán la energía fijada discriminando las prioridades que usted le asigne, incluyendo el consumo de agua caliente.</li>
            </ul>
          </div>
          <div class="col-md-6">
            <ul>
              <li class="padding-l-30 bolo-listado font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">La inteligente utilización de las tarifas nocturnas, donde podemos llegar a ahorrar hasta un 50 %.</li>
              <li class="padding-l-30 bolo-listado font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">La sencillez de nuestra instalación. Con lo que usted invertiría en la instalación de otros sistemas de calefacción, tendrá su calefacción pagada varios años con Newport.</li>
            </ul>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12 offset-lg-2 col-lg-8">
          <p class="font-size-s font-weight-n text-align-c color-w-0100 margin-b-18"><strong class="c-secondary">Recientemente la prestigiosa BRE del Reino Unido (Building Research Establishment) ha realizado un test comparando nuestro sistema con algunos de los más populares sistemas, como acumuladores  nocturnos, gas o radiadores eléctricos tradicionales. Los resultados hablan por sí solos:</strong></p>
        </div>
      </div>
      <div class="row">
        <div class="margin-b-40 overflow-h">
          <div class="col-md-6">
            <ul>
              <li class="padding-l-30 bolo-listado font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">Los radiadores Newport sólo consumen 1.16 kw a la hora para mantener una vivienda a 21 grados durante 24 horas, con una temperatura exterior de 5 grados.</li>
              <li class="padding-l-30 bolo-listado font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">Son los que logran mantener mejor la temperatura de consigna, sin fluctuaciones.</li>
              <li class="padding-l-30 bolo-listado font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">Lo acumuladores nocturnos fueron incapaces de mantener una temperatura de consigna de 21 grados, con grandes fluctuaciones en las habitaciones, que llegaron a situarse en los 30 grados. Eso implica un gasto energético enorme y, por supuesto, ineficiente.</li>
              <li class="padding-l-30 bolo-listado font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">La caldera de gas -equipada con radiadores de agua y válvula termostática  en cada radiador- necesitó 1.90 kw a la hora para calentar las habitaciones, debido entre otras causas, a las pérdidas de calor y energía que existen desde la caldera hasta el radiador situado en la habitación. Además, la temperatura variaba en las habitaciones entre los 20 y los 23 grados, a diferencia de los radiadores Newport, capaces de mantener los 21 grados con gran precisión.</li>
            </ul>
            <span class="luto-izq margin-b-30 display-b width-0100">
              <a class="btn-s-b" target="_blank" href="<?php echo get_template_directory_uri(); ?>/test-report-bre-estudio.pdf">Descargar estudio completo</a>
            </span>
          </div>
          <div class="col-md-6">
            <img class="img-responsive margin-b-18" src="<?php echo get_template_directory_uri(); ?>/img/grafica-kw-consumidos.png" alt="Gráfica ahorro kw consumidos">
          </div>
        </div>
      </div>

    </div>
  </article>
  <!-- FIN article 04 -->

  <!-- article 05 -->
  <article id="compacto-silicio" class="articulo padding-b-50">
    <div class="row">
      <header class="art-cabecera">
        <div class="col-md-12">
          <h2 class="art-num num-05 font-size-xl font-weight-l text-align-c color-w-0100">Compacto de silicio</h2>
        </div>
      </header>
    </div>
    <!-- art-division -->
    <div class="art-cuerpo">
      <div class="row">
        <div class="margin-b-40 overflow-h">
          <div class="col-md-6">
            <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">Nuestros equipos Newport incorporan un compactado compuesto fundamentalmente de óxido de silicio (un gran acumulador térmico) y óxido de aluminio (un gran transmisor). De esta combinación surge un material ideal para su aplicación en la calefacción, por su capacidad de acumulación y transmisión.</p>
            <div class="owl-carousel">
              <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/maxima-dureza-compacto-silicio-acumulacion.jpg" alt="Fabricación compacto de silicio máxima resistencia"></div>
              <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/maxima-dureza-compacto-silicio-fabricacion.jpg" alt="Fabricación compacto de silicio máxima resistencia"></div>
              <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/maxima-dureza-compacto-silicio-resistencia.jpg" alt="Fabricación compacto de silicio máxima resistencia"></div>
              <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/compacto-silicio-silex-perla.jpg" alt="Compacto silicio silex perla"></div>
              <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/compacto-silicio-silex-perla-cocina.jpg" alt="Compacto silicio silex perla cocina"></div>
              <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/compacto-silicio-silex-dark-cocina.jpg" alt="Compacto silicio silex dark cocina"></div>
            </div>
          </div>
          <div class="col-md-6">
            <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">Para lograr nuestro compacto, las arcillas ricas en silicio y aluminio son sometidas a presiones de 400 kg por cm2, antes de iniciar el proceso de cocción a más de 1.200° C  durante varias horas, donde alcanzan un estado semilíquido. Así se  permite  homogeneizar el material, logrando una altísima densidad natural, sin resinas ni ligantes; sólo utilizando presión y elevadas temperaturas.</p>
            <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">Finalmente cada pieza es sometida a un estricto control de calidad fotográfico y planimétrico, que detectan cualquier fisura o alteración en el acabado final.</p>
          </div>
        </div>
      </div>
    </div>
  </article>
  <!-- FIN article 05 -->

</div>
<!-- FIN container -->

</section>
<!-- FIN cuerpo -->

<?php // get_sidebar(); ?>
<?php get_footer(); ?>
