<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header("lineas-de-bano-casa"); ?>

<section id="cuerpo" class="bg-color-p-0100">
      <div class="container">

          <!-- article 01 -->
          <article class="articulo padding-b-50">
            <div class="row">
              <header class="art-cabecera">
                <div class="col-md-12">
                  <h2 class="art-num num-01 font-size-xl font-weight-l text-align-c color-w-0100">Ahorre energía y dinero</h2>
                </div>
              </header>
            </div>
            <!-- art-division -->
            <div class="art-cuerpo">
              <div class="row">
                <div class="offset-md-2 col-md-8">
                  <p class="font-size-s font-weight-n text-align-c color-w-0100 margin-b-30"><strong class="c-secondary">Programe su toallero</strong> para calentar el baño cada mañana, justo antes de utilizarlo. No malgaste dinero toda la noche.</p>
                </div>
                <div class="col-md-12">
                  <div class="art-cuerpo">
                    <div class="row">
                      <div class="col-md-3">
                        <h3 class="font-size-m font-weight-b text-align-l color-w-0100 margin-b-10">Previene</h3>
                        <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18 luto-izq"><strong class="c-secondary">Ayuda a prevenir</strong> problemas de humedad y condensación en su baño o aseo.</p>
                      </div>
                      <div class="col-md-6">
                        <img class="img-responsive margin-b-18" src="<?php echo get_template_directory_uri(); ?>/img/toallero-wifi.jpg" alt="Toallero wifi">
                      </div>
                      <div class="col-md-3">
                        <h3 class="font-size-m font-weight-b text-align-l color-w-0100 margin-b-10">Tecnología wifi</h3>
                        <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18 luto-izq">Tecnología wifi que le permite el <strong class="c-secondary">control total</strong> desde su tablet o smartphone desde donde usted esté en cada momento.</p>
                      </div>
                      <div class="offset-md-4 col-md-3">
                        <!-- <h3 class="font-size-m font-weight-b text-align-l color-w-0100 margin-t-20 margin-b-10">Lorem ipsum</h3> -->
                        <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18 luto-izq">Su toalla no volverá a estar fría ni húmeda.</p>
                      </div>
                      <!-- <div class="col-md-12">
                        <span class="display-b text-align-c">
                          <a class="btn-s-b" href="informacion-tecnica.html">Información técnica</a>
                        </span>
                      </div> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </article>
          <!-- FIN article 01 -->

          <!-- article 02 -->
          <article class="articulo padding-b-50">
            <div class="row">
              <header class="art-cabecera">
                <div class="col-md-12">
                  <h2 class="art-num num-02 font-size-xl font-weight-l text-align-c color-w-0100">Diseño</h2>
                </div>
              </header>
            </div>
            <!-- art-division -->
            <div class="art-cuerpo">
              <div class="row">
                <div class="col-md-12 offset-lg-2 col-lg-8">
                  <p class="font-size-s font-weight-n text-align-c color-w-0100 margin-b-30">Son <strong class="c-secondary">perfectos</strong>, por su diseño, para integrarse en la estética de cualquier cuarto de baño.</p>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 offset-lg-2 col-lg-8">
                  <div class="owl-carousel">
                      <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/toallero-solucion-ideal-bano-silex-perla.jpg" alt="Toallero baño"></div>
                      <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/toallero-solucion-ideal-bano-silex-nature-vertical.jpg" alt="Toallero baño"></div>
                      <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/toallero-solucion-ideal-bano-silex-slate-gris.jpg" alt="Toallero baño"></div>
                      <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/toallero-solucion-ideal-bano-silex-slate-vertical.jpg" alt="Toallero baño"></div>
                  </div>
                </div>
              </div>
            </div>
          </article>
          <!-- FIN article 02 -->

      </div>
      <!-- FIN container -->
    </section>
    <!-- FIN cuerpo -->

<?php // get_sidebar(); ?>
<?php get_footer(); ?>
