<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header("presupuesto-gratuito"); ?>

<section id="cuerpo" class="bg-color-p-0100">
      <div class="container">
        <div class="row">
          <form id="form-estudio" class="formulario overflow-h padding-t-50 padding-b-50" method="post" role="form">
            <div class="col-md-6">
              <div class="form-group padding-t-10 padding-b-10">
                <label class="color-w-0100 display-b font-size-s margin-b-4" for="nombre_contacto">Nombre</label>
                <input type="text" class="form-control width-0100 font-size-s" id="nombre_contacto" name="nombre_contacto" value="" required>
              </div>
              <div class="form-group padding-t-10 padding-b-10">
                <label class="color-w-0100 display-b font-size-s margin-b-4" for="email_contacto">Email</label>
                <input type="email" class="form-control width-0100 font-size-s" id="email_contacto" name="email_contacto" value="" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group padding-t-10 padding-b-10">
                <label class="color-w-0100 display-b font-size-s margin-b-4" for="tel_contacto">Teléfono</label>
                <input type="text" class="form-control width-0100 font-size-s" id="tel_contacto" name="tel_contacto" value="" required>
              </div>
              <div class="form-group padding-t-10 padding-b-10">
                <label class="color-w-0100 display-b font-size-s margin-b-4" for="ciudad_contacto">Ciudad</label>
                <input type="text" class="form-control width-0100 font-size-s" id="ciudad_contacto" name="ciudad_contacto" value="" required>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group padding-t-10 padding-b-10">
                <span class="display-b text-align-r">
                  <button type="submit" id="enviar_estudio" class="btn-s-b border-6" name="enviar_estudio">Enviar</button>
                </span>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!-- FIN container -->
    </section>
    <!-- FIN cuerpo -->

<?php // get_sidebar(); ?>
<?php get_footer("contacto"); ?>
