<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header("radiadores-piso"); ?>

<section id="cuerpo" class="bg-color-p-0100">
      <div class="container">

          <!-- article 01 -->
          <article class="articulo padding-b-50">
            <div class="row">
              <header class="art-cabecera">
                <div class="col-md-12">
                  <h2 class="art-num num-01 font-size-xl font-weight-l text-align-c color-w-0100">Compactado de silicio</h2>
                </div>
              </header>
            </div>
            <!-- art-division -->
            <div class="art-cuerpo">
              <div class="row">
                <div class="offset-md-2 col-md-8">
                  <p class="font-size-s font-weight-n text-align-c color-w-0100 margin-b-30">Nuestro <strong class="c-secondary">compactado de silicio</strong> equivale a una superficie de suelo radiante de hasta 36m2. Se instala sin obras. Calienta 10 veces más rápido, reacciona 10 veces mejor en calentamiento y es 10 veces mejor en ajuste de temperatura que el suelo radiante. Calor 100% uniforme.</p>
                </div>
                <div class="col-md-12">
                  <div class="art-cuerpo">
                    <div class="row">
                      <div class="col-md-3">
                        <!-- <h3 class="font-size-m font-weight-b text-align-l color-w-0100 margin-b-10">Lorem ipsum</h3> -->
                        <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18 luto-izq">Dureza y resistencia al rayado, con un 9 en la escala de Mohs, además de ser inalterable a la luz.</p>
                      </div>
                      <div class="col-md-6">
                        <div class="owl-carousel">
                          <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/compacto-silicio-equivale-36m-suelo-radiante.png" alt="Compactado de silicio equivale a una superficie de suelo radiante de hasta 36m2"></div>
                          <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/compacto-silicio-mas-rapido-mejor-reaccion-mejor-ajuste.png" alt="Calienta 10 veces más rápido, reacciona 10 veces mejor en calentamiento y es 10 veces mejor en ajuste de temperatura que el suelo radiante"></div>
                          <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/compacto-silicio-calor-uniforme.png" alt="Calor 100% uniforme"></div>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <!-- <h3 class="font-size-m font-weight-b text-align-l color-w-0100 margin-b-10">Lorem ipsum</h3> -->
                        <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18 luto-izq">Garantía de por vida en el compacto de silicio de todos nuestros equipos.</p>
                      </div>
                      <!-- <div class="offset-md-4 col-md-3">
                        <h3 class="font-size-m font-weight-b text-align-l color-w-0100 margin-t-20 margin-b-10">Lorem ipsum</h3>
                        <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18 luto-izq">Suspendisse id gravida augue, id varius ante. <strong class="c-secondary">Aliquam ut sem</strong> aliquam, rhoncus odio quis, dictum neque.</p>
                      </div> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </article>
          <!-- FIN article 01 -->


          <!-- article 02 -->
          <article class="articulo padding-b-50">
            <div class="row">
              <header class="art-cabecera">
                <div class="col-md-12">
                  <h2 class="art-num num-02 font-size-xl font-weight-l text-align-c color-w-0100">100% wifi automático y programable</h2>
                </div>
              </header>
            </div>
            <!-- art-division -->
            <div class="art-cuerpo">
              <div class="row">
                <div class="col-md-6">
                  <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">Se gestionan mediante un sistema <strong class="c-secondary">100% wifi automático y programable</strong>, que se adapta a todas las superficies.</p>
                  <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18 luto-izq"><strong class="c-secondary">Controles abatibles</strong> y gestión desde cualquier tablet o smartphone.</p>
                  <!--div class="row">
                    <div class="col-md-4">
                      <img class="img-responsive margin-b-18" src="img/wifi-calefaccion-salon.jpg" alt="Sistema wifi">
                    </div>
                    <div class="col-md-4">
                      <img class="img-responsive margin-b-18" src="img/wifi-calefaccion-habitacion.jpg" alt="Sistema wifi">
                    </div>
                    <div class="col-md-4">
                      <img class="img-responsive margin-b-18" src="img/wifi-calefaccion-habitacion-principal.jpg" alt="Sistema wifi">
                    </div>
                  </div-->
                </div>
                <div class="col-md-6">
                  <img class="img-responsive margin-b-18" src="<?php echo get_template_directory_uri(); ?>/img/sistema-wifi-personalizacion-temperatura-prioridades-predictivo.png" alt="Sistema wifi">
                </div>
              </div>

              <div class="row">
                <div class="col-md-12 offset-lg-3 col-lg-6">
                  <div class="owl-carousel">
                      <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/wifi-calefaccion-salon.jpg" alt="Sistema wifi"></div>
                      <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/wifi-calefaccion-habitacion.jpg" alt="Sistema wifi"></div>
                      <div><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/toallero-wifi.jpg" alt="Sistema wifi"></div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12 offset-lg-2 col-lg-8">
                  <span class="display-b text-align-c margin-t-18">
                    <a class="btn-s-b" href="<?php echo get_site_url(); ?>/informacion-tecnica">Información técnica</a>
                  </span>
                </div>
              </div>
            </div>
          </article>
          <!-- FIN article 02 -->

          <!-- article 03 -->
          <article class="articulo padding-b-50">
            <div class="row">
              <header class="art-cabecera">
                <div class="col-md-12">
                  <h2 class="art-num num-03 font-size-xl font-weight-l text-align-c color-w-0100">Acabados</h2>
                </div>
              </header>
            </div>
            <!-- art-division -->
            <div class="art-cuerpo">
              <div class="row">
                <div class="offset-md-2 col-md-8">
                  <p class="font-size-s font-weight-n text-align-c color-w-0100 margin-b-30">Nuestros radiadores están disponibles en formato 60 cm x 60 cm en acabado <strong class="c-secondary">Silex Perla</strong> y en los acabados <strong class="c-secondary">Premium</strong>, el <strong class="c-secondary">Silex Dark</strong> y  el <strong class="c-secondary">Silex Nature</strong>. El estilizado equipo vertical de 110 cm de alto por 45 de ancho está disponible en acabado Perla y en los acabados Premium Silex Slate y Nature. Además, los acabados Perla pueden ser pintados con el color que usted prefiera, integrándose totalmente en la decoración de su habitación.</p>
                </div>
              </div>

              <div class="row">

                <!-- <div class="col-sm-6 col-md-2">
                  <a data-toggle="modal" data-target="#exampleModal" class="text-align-c" href="#">
                    <img class="img-responsive" src="img/silex-perla.png" alt="Silex Perla 60x60">
                    <span class="display-b margin-t-10 font-size-xs font-weight-l text-align-c color-w-0100 margin-b-30">Silex Perla 60x60</span>
                  </a>
                </div> -->

                <!-- modal Silex Perla 60x60 -->
                <!-- <div class="modal fade z-index-total" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title float-l font-size-l color-p-0100" id="exampleModalLabel"><strong>Silex Perla 60x60</strong></h5>
                        <a type="button" class="float-r" data-dismiss="modal" aria-label="Close">
                          <span class="btn-s-b display-b text-align-c" aria-hidden="true">&times;</span>
                        </a>
                      </div>
                      <div class="modal-body">
                        <img class="img-responsive margin-b-10" src="img/estanteria-silex-perla-detalle.jpg" alt="Silex Perla 60x60">
                        <p class="font-size-xs font-weight-n text-align-l margin-b-10">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut enim sollicitudin, scelerisque dolor quis, tincidunt ipsum. Sed a molestie dolor. Donec id vehicula dolor. Fusce sollicitudin nisi malesuada sem feugiat, suscipit volutpat elit pellentesque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum vitae nibh dictum, placerat dolor in, finibus odio. Aliquam sit amet nulla non lacus posuere porttitor ut ac turpis. Proin in massa a nunc iaculis gravida lacinia a tellus. Cras laoreet ornare sodales. Fusce auctor tellus at dolor viverra pellentesque. Fusce dolor odio, finibus a lacus sit amet, imperdiet volutpat enim. Maecenas nibh dui, sollicitudin vitae justo quis, dignissim sodales odio. Donec pellentesque mi eget ex convallis commodo. Sed aliquet consequat odio, in suscipit nisl aliquet ut. Fusce ut orci at mi posuere euismod nec in elit.</p> 
                      </div>
                      <div class="modal-footer">
                        <a type="button" class="float-r" data-dismiss="modal" aria-label="Close">
                          <span class="btn-s-b display-b text-align-c" aria-hidden="true">Cerrar</span>
                        </a>
                      </div>
                    </div>
                  </div>
                </div> -->
                <!-- FIN modal Silex Perla 60x60 -->

                <!-- imageGallery1 -->
                <div class="imageGallery1">


                   <!-- <a href="http://lokeshdhakar.com/projects/lightbox2/images/image-3.jpg" title="Caption for gallery item 1"><img src="http://lokeshdhakar.com/projects/lightbox2/images/thumb-3.jpg" alt="Gallery image 1" /></a>
                   <a href="http://lokeshdhakar.com/projects/lightbox2/images/image-4.jpg" title="Caption for gallery item 2"><img src="http://lokeshdhakar.com/projects/lightbox2/images/thumb-4.jpg" alt="Gallery image 2" /></a>
                   <a href="http://lokeshdhakar.com/projects/lightbox2/images/image-5.jpg" title="Caption for gallery item 3"><img src="http://lokeshdhakar.com/projects/lightbox2/images/thumb-5.jpg" alt="Gallery image 3" /></a> -->
                

                  <div class="col-sm-6 col-md-2">
                    <a class="text-align-c" href="<?php echo get_template_directory_uri(); ?>/img/silex-perla-radiador.jpg" title="Silex Perla 60x60">
                      <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/silex-perla.png" alt="Silex Perla 60x60">
                      <span class="display-b margin-t-10 font-size-xs font-weight-l text-align-c color-w-0100 margin-b-20">Silex Perla 60x60</span>
                    </a>
                    <div class="row">
                      <div class="col-md-6">
                          <a href="<?php echo get_template_directory_uri(); ?>/img/silex-perla-despacho.jpg" title="Silex Perla 60x60"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/silex-perla-despacho.jpg" alt="Silex Perla 60x60" /></a>
                      </div>
                      <div class="col-md-6">
                          <a href="<?php echo get_template_directory_uri(); ?>/img/silex-perla-oficina.jpg" title="Silex Perla 60x60"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/silex-perla-oficina.jpg" alt="Silex Perla 60x60" /></a>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6 col-md-2">
                    <a class="text-align-c" href="<?php echo get_template_directory_uri(); ?>/img/silex-perla-vertical-radiador.jpg" title="Silex Perla 45x110">
                      <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/silex-perla-vertical.png" alt="Silex Perla 45x110">
                      <span class="display-b margin-t-10 font-size-xs font-weight-l text-align-c color-w-0100 margin-b-20">Silex Perla 45x110</span>
                    </a>
                    <div class="row">
                      <div class="col-md-6">
                          <a href="<?php echo get_template_directory_uri(); ?>/img/estanteria-silex-perla-vertical.jpg" title="Silex Perla 45x110"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/estanteria-silex-perla-vertical.jpg" alt="Silex Perla 45x110" /></a>
                      </div>
                      <div class="col-md-6">
                          <a href="<?php echo get_template_directory_uri(); ?>/img/silex-perla-vertical-oficina.jpg" title="Silex Perla 45x110"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/silex-perla-vertical-oficina.jpg" alt="Silex Perla 45x110" /></a>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6 col-md-2">
                    <a class="text-align-c" href="<?php echo get_template_directory_uri(); ?>/img/silex-dark-radiador.jpg" title="Silex Dark 60x60">
                      <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/silex-dark.png" alt="Silex Dark 60x60">
                      <span class="display-b margin-t-10 font-size-xs font-weight-l text-align-c color-w-0100 margin-b-20">Silex Dark 60x60</span>
                    </a>
                    <div class="row">
                      <div class="col-md-6">
                          <a href="<?php echo get_template_directory_uri(); ?>/img/silex-dark-salon.jpg" title="Silex Dark 60x60"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/silex-dark-salon.jpg" alt="Silex Dark 60x60" /></a>
                      </div>
                      <div class="col-md-6">
                          <a href="<?php echo get_template_directory_uri(); ?>/img/silex-dark-oficina.jpg" title="Silex Dark 60x60"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/silex-dark-oficina.jpg" alt="Silex Dark 60x60" /></a>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6 col-md-2">
                    <a class="text-align-c" href="<?php echo get_template_directory_uri(); ?>/img/silex-dark-vertical-radiador.jpg" title="Silex Dark 45x110">
                      <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/silex-dark-vertical.png" alt="Silex Dark 45x110">
                      <span class="display-b margin-t-10 font-size-xs font-weight-l text-align-c color-w-0100 margin-b-20">Silex Dark 45x110</span>
                    </a>
                    <div class="row">
                      <div class="col-md-6">
                          <a href="<?php echo get_template_directory_uri(); ?>/img/silex-dark-vertical-salon.jpg" title="Silex Dark 45x110"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/silex-dark-vertical-salon.jpg" alt="Silex Dark 45x110" /></a>
                      </div>
                      <div class="col-md-6">
                          <a href="<?php echo get_template_directory_uri(); ?>/img/estanteria-silex-dark-vertical.jpg" title="Silex Dark 45x110"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/estanteria-silex-dark-vertical.jpg" alt="Silex Dark 45x110" /></a>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6 col-md-2">
                    <a class="text-align-c" href="<?php echo get_template_directory_uri(); ?>/img/silex-nature-radiador.jpg" title="Silex Nature 60x60">
                      <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/silex-nature.png" alt="Silex Nature 60x60">
                      <span class="display-b margin-t-10 font-size-xs font-weight-l text-align-c color-w-0100 margin-b-20">Silex Nature 60x60</span>
                    </a>
                    <div class="row">
                      <div class="col-md-6">
                          <a href="<?php echo get_template_directory_uri(); ?>/img/silex-nature-salon.jpg" title="Silex Nature 60x60"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/silex-nature-salon.jpg" alt="Silex Nature 60x60" /></a>
                      </div>
                      <div class="col-md-6">
                          <a href="<?php echo get_template_directory_uri(); ?>/img/silex-nature-despacho.jpg" title="Silex Nature 60x60"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/silex-nature-despacho.jpg" alt="Silex Nature 60x60" /></a>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6 col-md-2">
                    <a class="text-align-c" href="<?php echo get_template_directory_uri(); ?>/img/silex-nature-vertical-radiador.jpg" title="Silex Nature 45x110">
                      <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/silex-nature-vertical.png" alt="Silex Nature 45x110">
                      <span class="display-b margin-t-10 font-size-xs font-weight-l text-align-c color-w-0100 margin-b-20">Silex Nature 45x110</span>
                    </a>
                    <div class="row">
                      <div class="col-md-6">
                          <a href="<?php echo get_template_directory_uri(); ?>/img/estanteria-silex-nature-vertical.jpg" title="Silex Nature 45x110"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/estanteria-silex-nature-vertical.jpg" alt="Silex Nature 45x110" /></a>
                      </div>
                      <div class="col-md-6">
                          <a href="<?php echo get_template_directory_uri(); ?>/img/silex-nature-vertical-salon.jpg" title="Silex Nature 45x110"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/silex-nature-vertical-salon.jpg" alt="Silex Nature 45x110" /></a>
                      </div>
                    </div>
                  </div>

                </div>
                <!-- FIN imageGallery1 -->

              </div>
            </div>

          </article>
          <!-- FIN article 03 -->

      </div>
      <!-- FIN container -->
    </section>
    <!-- FIN cuerpo -->

<?php // get_sidebar(); ?>
<?php get_footer(); ?>
