<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header("sobre-newport"); ?>

<section id="cuerpo" class="bg-color-p-0100">
      <div class="container">

          <!-- article 01 -->
          <article class="articulo padding-b-50">
            <div class="row">
              <header class="art-cabecera">
                <div class="col-md-12">
                  <h2 class="art-num num-01 font-size-xl font-weight-l text-align-c color-w-0100">Bienvenido a Newport</h2>
                </div>
              </header>
            </div>
            <!-- art-division -->
            <div class="art-cuerpo">
              <div class="row">
                <div class="col-md-12">
                  <p class="font-size-s font-weight-n text-align-c color-w-0100 margin-b-18">Tras su <strong class="c-secondary">éxito en el mercado</strong> Francés, Inglés y Alemán</p>

                  <div class="row">
                    <div class="offset-md-2 col-md-8 offset-lg-4 col-lg-4">
                      <div class="row">
                        <ul class="overflow-h margin-b-20">
                          <li class="display-i">
                            <div class="col-xs-4">
                              <img class="img-responsive padding-b-10" src="<?php echo get_template_directory_uri(); ?>/img/bandera-francia.png" alt="Bandera Francia">
                            </div>
                          </li>
                          <li class="display-i">
                            <div class="col-xs-4">
                              <img class="img-responsive padding-b-10" src="<?php echo get_template_directory_uri(); ?>/img/bandera-reino-unido.png" alt="Bandera Reino Unido">
                            </div>
                          </li>
                          <li class="display-i">
                            <div class="col-xs-4">
                              <img class="img-responsive padding-b-10" src="<?php echo get_template_directory_uri(); ?>/img/bandera-alemania.png" alt="Bandera Alemania">
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>

                  <p class="font-size-s font-weight-n text-align-c color-w-0100 margin-b-30">
                    <span class="display-b">con más de  <strong class="font-size-xl">500.000</strong> <strong class="c-secondary">unidades vendidas</strong>,</span>
                    <span class="display-b margin-b-16">lanza en <strong class="c-secondary">España</strong> el <strong class="font-size-xl">Calor Híbrido</strong></span>
                    <span class="display-b">El <strong class="c-secondary">único en el mercado</strong> que reúne lo mejor de todos los demás sistemas de calefacción y agua caliente:</span> 
                  </p>

                  <div class="row">
                    <ul class="overflow-h margin-b-20">
                      <li class="display-i">
                        <div class="col-sm-6 col-md-3">
                          <img class="display-b margin-0-auto-10-auto" src="<?php echo get_template_directory_uri(); ?>/img/icono-economia-aerotermia.png" alt="Icono Economía de la Aerotermia">
                          <strong class="display-b font-size-s font-weight-b text-align-c color-w-0100 margin-b-20">Economía de la Aerotermia</strong>
                        </div>
                      </li>
                      <li class="display-i">
                        <div class="col-sm-6 col-md-3">
                          <img class="display-b margin-0-auto-10-auto" src="<?php echo get_template_directory_uri(); ?>/img/icono-confort-gas-agua-caliente.png" alt="Icono Confort del gas en agua caliente">
                          <strong class="display-b font-size-s font-weight-b text-align-c color-w-0100 margin-b-20">Confort del gas en agua caliente</strong>
                        </div>
                      </li>
                      <li class="display-i">
                        <div class="col-sm-6 col-md-3">
                          <img class="display-b margin-0-auto-10-auto" src="<?php echo get_template_directory_uri(); ?>/img/icono-estetica-suelo-radiante.png" alt="Icono Estética de suelo radiante">
                          <strong class="display-b font-size-s font-weight-b text-align-c color-w-0100 margin-b-20">Estética de suelo radiante</strong>
                        </div>
                      </li>
                      <li class="display-i">
                        <div class="col-sm-6 col-md-3">
                          <img class="display-b margin-0-auto-10-auto" src="<?php echo get_template_directory_uri(); ?>/img/icono-facil-instalacion.png" alt="Icono Facilidad de instalación de la calefacción eléctrica">
                          <strong class="display-b font-size-s font-weight-b text-align-c color-w-0100 margin-b-20">Facilidad de instalación de la calefacción eléctrica</strong>
                        </div>
                      </li>
                      <li class="display-i">
                        <div class="col-sm-6 col-md-3">
                          <img class="display-b margin-0-auto-10-auto" src="<?php echo get_template_directory_uri(); ?>/img/icono-modular-sin-obras.png" alt="Icono 100% modular y sin obras">
                          <strong class="display-b font-size-s font-weight-b text-align-c color-w-0100 margin-b-20">100% modular y sin obras</strong>
                        </div>
                      </li>
                      <li class="display-i">
                        <div class="col-sm-6 col-md-3">
                          <img class="display-b margin-0-auto-10-auto" src="<?php echo get_template_directory_uri(); ?>/img/icono-tarifas-reducidas.png" alt="Icono Tarifas reducidas">
                          <strong class="display-b font-size-s font-weight-b text-align-c color-w-0100 margin-b-20">Tarifas reducidas</strong>
                        </div>
                      </li>
                      <li class="display-i">
                        <div class="col-sm-6 col-md-3">
                          <img class="display-b margin-0-auto-10-auto" src="<?php echo get_template_directory_uri(); ?>/img/icono-control-total.png" alt="Icono Control total">
                          <strong class="display-b font-size-s font-weight-b text-align-c color-w-0100 margin-b-20">Control total</strong>
                        </div>
                      </li>
                      <li class="display-i">
                        <div class="col-sm-6 col-md-3">
                          <img class="display-b margin-0-auto-10-auto" src="<?php echo get_template_directory_uri(); ?>/img/icono-personalizacion.png" alt="Icono Personalización">
                          <strong class="display-b font-size-s font-weight-b text-align-c color-w-0100 margin-b-20">Personalización</strong>
                        </div>
                      </li>
                      <li class="display-i text-align-c">
                        <div class="col-sm-12 col-md-12">
                          <img class="display-b margin-0-auto-10-auto" src="<?php echo get_template_directory_uri(); ?>/img/icono-ecologia-energia-solar.png" alt="Icono ecología energía solar">
                          <strong class="display-b font-size-s font-weight-b text-align-c color-w-0100 margin-b-20">Ecología energía solar</strong>
                        </div>
                      </li>
                    </ul>
                  </div>

                  <span class="display-b text-align-c">
                    <a class="btn-s-b" href="<?php echo get_site_url(); ?>/informacion-tecnica">Información técnica</a>
                  </span>
                </div>
              </div>
            </div>
          </article>
          <!-- FIN article 01 -->

          <!-- article 02 -->
          <article class="articulo padding-b-50">
            <div class="row">
              <header class="art-cabecera">
                <div class="col-md-12">
                  <h2 class="art-num num-02 font-size-xl font-weight-l text-align-c color-w-0100">Certificado</h2>
                </div>
              </header>
            </div>
            <!-- art-division -->
            <div class="row">
              <div class="art-cuerpo">
                <div class="col-md-12 offset-lg-2 col-lg-8">
                  <p class="font-size-s font-weight-n text-align-c color-w-0100 margin-b-18">Nuestros equipos cuenta con la <strong class="c-secondary">certificación Europea de máxima calidad (NF Performance)</strong> que certifica un Coeficiente de Aptitud desde 0,13, con promedios entre 0,16 y 0,28.</p>

                  <div class="text-align-c margin-b-18">
                    <img class="display-b margin-0-auto-10-auto" src="<?php echo get_template_directory_uri(); ?>/img/certificado-bureau-veritas.png" alt="Sello certificado Bureau Veritas">
                  </div>

                  <p class="font-size-s font-weight-n text-align-c color-w-0100 margin-b-18">RAPPORT D’ESSAI nº 125126-641771 y 125125-651764 by Valderoma</p>

                  <p class="font-size-s font-weight-n text-align-c color-w-0100 margin-b-18">Nuestra calefacción eléctrica es <strong class="c-secondary">la más eficiente</strong> en calentamiento, mantenimiento de la temperatura y distribución del calor en la estancia.</p>

                  <span class="display-b text-align-c">
                    <a class="btn-s-b" href="<?php echo get_site_url(); ?>/informacion-tecnica">Información técnica</a>
                  </span>
                </div>
              </div>
            </div>
          </article>
          <!-- FIN article 02 -->

          <!-- article 03 -->
          <!-- <article class="articulo padding-b-50">
            <div class="row">
              <header class="art-cabecera">
                <div class="col-md-12">
                  <h2 class="art-num num-03 font-size-xl font-weight-l text-align-c color-w-0100">La Calefacción más Eficiente</h2>
                </div>
              </header>
            </div>
            <div class="row">
              <div class="art-cuerpo">
                <div class="col-md-6">
                  <img class="img-responsive margin-b-18" src="img/simula-img.jpg" alt="LoremIpsumDolor">
                </div>
                <div class="col-md-6">
                  <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18">Nuestra calefacción eléctrica es <strong class="c-secondary">la más eficiente</strong> en calentamiento, mantenimiento de la temperatura y distribución del calor en la estancia.</p>
                  <span class="luto-izq">
                    <a class="btn-s-b" href="informacion-tecnica.html">Información Técnica</a>
                  </span>
                </div>
              </div>
            </div>
          </article> -->
          <!-- FIN article 03 -->

          <!-- article 03 -->
          <article class="articulo padding-b-50">
            <div class="row">
              <header class="art-cabecera">
                <div class="col-md-12">
                  <h2 class="art-num num-03 font-size-xl font-weight-l text-align-c color-w-0100">Integración nuevas tecnologías</h2>
                </div>
              </header>
            </div>
            <!-- art-division -->
            <div class="art-cuerpo">
              <div class="row">
                <div class="col-md-3">
                  <h3 class="font-size-m font-weight-b text-align-l color-w-0100 margin-b-10">Limitación de potencia</h3>
                  <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18 luto-izq">Nuestro sistema permite al usuario decidir que potencia máxima quiere consumir, ajustándose exactamente a la potencia que se quiere contratar.</p>
                </div>
                <div class="col-md-6">
                  <img class="img-responsive margin-b-18" src="<?php echo get_template_directory_uri(); ?>/img/nuevas-tecnologias-calefaccion-bosque-tlf-movil-wifi.jpg" alt="Nuevas tecnologias calefaccion wifi">
                </div>
                <div class="col-md-3">
                  <h3 class="font-size-m font-weight-b text-align-l color-w-0100 margin-b-10">Agua caliente</h3>
                  <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18 luto-izq">Ahora también se gestiona en conjunto con su calefacción, interactuando del modo más eficiente, modulando el consumo de calefacción cuando usted necesita el agua caliente.</p>
                </div>
                <div class="offset-md-4 col-md-3">
                  <!-- <h3 class="font-size-m font-weight-b text-align-l color-w-0100 margin-t-20 margin-b-10">Lorem ipsum</h3> -->
                  <p class="font-size-s font-weight-n text-align-l color-w-0100 margin-b-18 luto-izq">Desde cualquier lugar, y a través de su smartphone, controle, gestione y supervise su calefacción. Por fin puede saber en cualquier momento cuanto lleva consumido en agua caliente y calefacción.</p>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 offset-lg-2 col-lg-8">
                  <span class="display-b text-align-c margin-t-18">
                    <a class="btn-s-b" href="<?php echo get_site_url(); ?>/informacion-tecnica">Información técnica</a>
                  </span>
                </div>
              </div>
            </div>
          </article>
          <!-- FIN article 03 -->

      </div>
      <!-- FIN container -->
    </section>
    <!-- FIN cuerpo -->

<?php // get_sidebar(); ?>
<?php get_footer(); ?>
