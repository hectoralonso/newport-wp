<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<?php
$thumb_id = get_post_thumbnail_id();
$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'newport', true);
$thumb_url = $thumb_url_array[0];
?>

<section id="cuerpo" class="bg-color-w-0100">
	<div class="container">
	    <div class="row padding-t-50 padding-b-30">
	        <article class="blog-detalle col-xs-12 offset-sm-1 col-sm-10 offset-md-4 col-md-8 offset-l-6 col-lg-6">
	        	<span class="font-size-xs font-weight-l text-align-l display-b margin-b-10">
		    		<?php echo esc_html( get_the_date() ); ?>
		    	</span>
		    	<header class="margin-b-10">
				    <h2 class="blog-titulo font-size-l font-weight-b text-align-i">
				    	<?php the_title(); ?>
				    <h2>
				</header>
				<div class="blog-parrafos">
					<?php the_content(); ?>
				</div>
				<div class="blog-img">
					<?php if ( has_post_thumbnail() ) { ?>
	                    <img class="img-responsive margin-b-20" src="<?php echo $thumb_url; ?>" alt="<?php  the_title(); ?>">
	                <?php } else { } ?>
				</div>
				<div class="margin-b-30">
				<?php echo do_shortcode('[ssba-buttons]'); ?>
				</div>
	        </article>
	    </div>
	</div>
</section>
